﻿
namespace Diplomski.Forme
{
    partial class DodajClanarinu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnSnimi = new System.Windows.Forms.Button();
            this.txtTipClanarine = new System.Windows.Forms.TextBox();
            this.labelTipClanarine = new System.Windows.Forms.Label();
            this.txtCenaClanarine = new System.Windows.Forms.TextBox();
            this.labelCenaClanarine = new System.Windows.Forms.Label();
            this.txtVremeVazenja = new System.Windows.Forms.TextBox();
            this.labelVremeVazenja = new System.Windows.Forms.Label();
            this.labelDatumDo = new System.Windows.Forms.Label();
            this.labelDatumOd = new System.Windows.Forms.Label();
            this.dtDatumOd = new System.Windows.Forms.DateTimePicker();
            this.dtDatumDo = new System.Windows.Forms.DateTimePicker();
            this.SuspendLayout();
            // 
            // btnSnimi
            // 
            this.btnSnimi.Location = new System.Drawing.Point(569, 360);
            this.btnSnimi.Name = "btnSnimi";
            this.btnSnimi.Size = new System.Drawing.Size(190, 45);
            this.btnSnimi.TabIndex = 24;
            this.btnSnimi.Text = "Snimi";
            this.btnSnimi.UseVisualStyleBackColor = true;
            this.btnSnimi.Click += new System.EventHandler(this.btnSnimi_Click);
            // 
            // txtTipClanarine
            // 
            this.txtTipClanarine.Location = new System.Drawing.Point(219, 151);
            this.txtTipClanarine.Name = "txtTipClanarine";
            this.txtTipClanarine.Size = new System.Drawing.Size(242, 26);
            this.txtTipClanarine.TabIndex = 21;
            this.txtTipClanarine.TextChanged += new System.EventHandler(this.txtTipClanarine_TextChanged);
            // 
            // labelTipClanarine
            // 
            this.labelTipClanarine.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTipClanarine.Location = new System.Drawing.Point(33, 151);
            this.labelTipClanarine.Name = "labelTipClanarine";
            this.labelTipClanarine.Size = new System.Drawing.Size(150, 97);
            this.labelTipClanarine.TabIndex = 20;
            this.labelTipClanarine.Text = "Tip clanarine";
            this.labelTipClanarine.Click += new System.EventHandler(this.labelTipClanarine_Click);
            // 
            // txtCenaClanarine
            // 
            this.txtCenaClanarine.Location = new System.Drawing.Point(219, 90);
            this.txtCenaClanarine.Name = "txtCenaClanarine";
            this.txtCenaClanarine.Size = new System.Drawing.Size(242, 26);
            this.txtCenaClanarine.TabIndex = 19;
            this.txtCenaClanarine.TextChanged += new System.EventHandler(this.txtCenaClanarine_TextChanged);
            // 
            // labelCenaClanarine
            // 
            this.labelCenaClanarine.AutoSize = true;
            this.labelCenaClanarine.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCenaClanarine.Location = new System.Drawing.Point(33, 90);
            this.labelCenaClanarine.Name = "labelCenaClanarine";
            this.labelCenaClanarine.Size = new System.Drawing.Size(163, 26);
            this.labelCenaClanarine.TabIndex = 18;
            this.labelCenaClanarine.Text = "Cena Clanarine";
            this.labelCenaClanarine.Click += new System.EventHandler(this.labelCenaClanarine_Click);
            // 
            // txtVremeVazenja
            // 
            this.txtVremeVazenja.Location = new System.Drawing.Point(219, 28);
            this.txtVremeVazenja.Name = "txtVremeVazenja";
            this.txtVremeVazenja.Size = new System.Drawing.Size(242, 26);
            this.txtVremeVazenja.TabIndex = 17;
            this.txtVremeVazenja.TextChanged += new System.EventHandler(this.txtVremeVazenja_TextChanged);
            // 
            // labelVremeVazenja
            // 
            this.labelVremeVazenja.AutoSize = true;
            this.labelVremeVazenja.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelVremeVazenja.Location = new System.Drawing.Point(33, 28);
            this.labelVremeVazenja.Name = "labelVremeVazenja";
            this.labelVremeVazenja.Size = new System.Drawing.Size(162, 26);
            this.labelVremeVazenja.TabIndex = 16;
            this.labelVremeVazenja.Text = "Vreme Vazenja";
            this.labelVremeVazenja.Click += new System.EventHandler(this.labelVremeVazenja_Click);
            // 
            // labelDatumDo
            // 
            this.labelDatumDo.AutoSize = true;
            this.labelDatumDo.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDatumDo.Location = new System.Drawing.Point(33, 266);
            this.labelDatumDo.Name = "labelDatumDo";
            this.labelDatumDo.Size = new System.Drawing.Size(140, 26);
            this.labelDatumDo.TabIndex = 25;
            this.labelDatumDo.Text = "Datum isteka";
            this.labelDatumDo.Click += new System.EventHandler(this.labelDatumDo_Click);
            // 
            // labelDatumOd
            // 
            this.labelDatumOd.AutoSize = true;
            this.labelDatumOd.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDatumOd.Location = new System.Drawing.Point(33, 207);
            this.labelDatumOd.Name = "labelDatumOd";
            this.labelDatumOd.Size = new System.Drawing.Size(159, 26);
            this.labelDatumOd.TabIndex = 26;
            this.labelDatumOd.Text = "Datum pocetka";
            this.labelDatumOd.Click += new System.EventHandler(this.labelDatumOd_Click);
            // 
            // dtDatumOd
            // 
            this.dtDatumOd.Location = new System.Drawing.Point(219, 207);
            this.dtDatumOd.Name = "dtDatumOd";
            this.dtDatumOd.Size = new System.Drawing.Size(242, 26);
            this.dtDatumOd.TabIndex = 27;
            this.dtDatumOd.ValueChanged += new System.EventHandler(this.dtDatumOd_ValueChanged);
            // 
            // dtDatumDo
            // 
            this.dtDatumDo.Location = new System.Drawing.Point(219, 266);
            this.dtDatumDo.Name = "dtDatumDo";
            this.dtDatumDo.Size = new System.Drawing.Size(242, 26);
            this.dtDatumDo.TabIndex = 28;
            this.dtDatumDo.ValueChanged += new System.EventHandler(this.dtDatumDo_ValueChanged);
            // 
            // DodajClanarinu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1089, 510);
            this.Controls.Add(this.dtDatumDo);
            this.Controls.Add(this.dtDatumOd);
            this.Controls.Add(this.labelDatumOd);
            this.Controls.Add(this.labelDatumDo);
            this.Controls.Add(this.btnSnimi);
            this.Controls.Add(this.txtTipClanarine);
            this.Controls.Add(this.labelTipClanarine);
            this.Controls.Add(this.txtCenaClanarine);
            this.Controls.Add(this.labelCenaClanarine);
            this.Controls.Add(this.txtVremeVazenja);
            this.Controls.Add(this.labelVremeVazenja);
            this.Name = "DodajClanarinu";
            this.Text = "DodajClanarinu";
            this.Load += new System.EventHandler(this.DodajClanarinu_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnSnimi;
        private System.Windows.Forms.TextBox txtTipClanarine;
        private System.Windows.Forms.Label labelTipClanarine;
        private System.Windows.Forms.TextBox txtCenaClanarine;
        private System.Windows.Forms.Label labelCenaClanarine;
        private System.Windows.Forms.TextBox txtVremeVazenja;
        private System.Windows.Forms.Label labelVremeVazenja;
        private System.Windows.Forms.Label labelDatumDo;
        private System.Windows.Forms.Label labelDatumOd;
        private System.Windows.Forms.DateTimePicker dtDatumOd;
        private System.Windows.Forms.DateTimePicker dtDatumDo;
    }
}