﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Diplomski.Entiteti;

using NHibernate;
using System.Globalization;

namespace Diplomski.Forme
{
    public partial class DodajRashod : Form
    {
        public DodajRashod()
        {
            InitializeComponent();
        }

        private void btnSnimi_Click(object sender, EventArgs e)
        {
            try
            {
                ISession s = DataLayer.GetSession();


                Rashod cl = new Rashod()
                {
                    Tip_Rashoda = txtTipRashoda.Text,
                    Cena = int.Parse(txtCena.Text),
                    Izdavaoc = txtIzdavaoc.Text,
                    DatumIzdavanja= DateTime.ParseExact(dtDatumIzdavanja.Value.ToString("dd-MM-yyyy"), "dd-MM-yyyy", CultureInfo.InvariantCulture),
 
                };

                s.Save(cl);
                s.Flush();
                s.Close();

                this.DialogResult = DialogResult.OK;

            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.ToString());
            }
        }
    }
}
