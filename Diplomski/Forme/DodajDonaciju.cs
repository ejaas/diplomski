﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Diplomski.Entiteti;
using NHibernate;
using System.Windows.Forms;

namespace Diplomski.Forme
{
    public partial class DodajDonaciju : Form
    {
        public DodajDonaciju()
        {
            InitializeComponent();
        }

        private void btnDodajDonaciju_Click(object sender, EventArgs e)
        {
            listBox1.Items.Add(txtDonacija.Text);
            txtDonacija.Text = "";
        }

        private void btnSnimi_Click(object sender, EventArgs e)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                foreach (string i in listBox1.Items)
                {
                    Prihod.Donacija p = new Prihod.Donacija()
                    {

                        Tip_Donacije = i
                    };

                    s.Save(p);
                    s.Flush();
                }

                s.Close();
                this.DialogResult = DialogResult.OK;
            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.ToString());
            }
        }
    }
}
