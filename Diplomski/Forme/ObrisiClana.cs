﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Diplomski.Entiteti;
using NHibernate;
using static Diplomski.DTOs;

namespace Diplomski.Forme
{
    public partial class ObrisiClana : Form
    {
        Clan cl;
        public ObrisiClana()
        {
            InitializeComponent();
            cl = null;
        }

        private void ObrisiClana_Load(object sender, EventArgs e)
        {
            this.PopuniListuClanova();
        }
        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void PopuniListuClanova()
        {
           
                IList<ClanoviZaBrisanje> clanovi = DTOManager.VratiClanoveZaBrisanjeDTO();

                foreach (ClanoviZaBrisanje clan in clanovi)
                {
                    ListViewItem item = new ListViewItem(new string[] { clan.IdClana.ToString(), clan.Ime, clan.Prezime });

                    listView1.Items.Add(item);
                }
                listView1.Refresh();
            
        }

        private void btnObrisi_Click(object sender, EventArgs e)
        {
            if (listView1.SelectedItems.Count > 0)
            {
    
                try
                {
                    ISession s = DataLayer.GetSession();
                    Clan clan = s.Load<Clan>(int.Parse(listView1.SelectedItems[0].Text));
                    s.Flush();
                    s.Delete(clan);
                    s.Flush();

                    s.Close();
                }
                catch (Exception ec)
                {
                    MessageBox.Show(ec.ToString());
                }
                this.DialogResult = DialogResult.OK;

            }
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                IQuery q = s.CreateQuery("from Clan");

                // IEnumerable<Clan> clanovi = q.Enumerable<Clan>();
                IList<ClanInfo> clanovi = DTOManager.VratiClanove();

                listView1.Items.Clear();

                foreach (ClanInfo n in clanovi)
                {
                    if (n.Ime.Substring(0,1) == txtString.Text)
                    {
                        listView1.Refresh();
                ListViewItem item = new ListViewItem(new string[] { n.IdClan.ToString(), n.Ime, n.Prezime, n.Email, n.Telefon.ToString(), n.Adresa, n.Rod, n.Jmbg.ToString(), n.brLk.ToString(), n.Imeroditelja });

                        listView1.Items.Add(item);
                    }
                }
                listView1.Refresh();

                

                s.Close();

            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }
          
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void txtString_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
