﻿
namespace Diplomski.Forme
{
    partial class DodajRashod
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dtDatumIzdavanja = new System.Windows.Forms.DateTimePicker();
            this.labelDatumIzdavanja = new System.Windows.Forms.Label();
            this.btnSnimi = new System.Windows.Forms.Button();
            this.txtTipRashoda = new System.Windows.Forms.TextBox();
            this.labelTipRashoda = new System.Windows.Forms.Label();
            this.txtIzdavaoc = new System.Windows.Forms.TextBox();
            this.labelIzdavaoc = new System.Windows.Forms.Label();
            this.txtCena = new System.Windows.Forms.TextBox();
            this.labelCena = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // dtDatumIzdavanja
            // 
            this.dtDatumIzdavanja.Location = new System.Drawing.Point(235, 194);
            this.dtDatumIzdavanja.Name = "dtDatumIzdavanja";
            this.dtDatumIzdavanja.Size = new System.Drawing.Size(242, 26);
            this.dtDatumIzdavanja.TabIndex = 38;
            // 
            // labelDatumIzdavanja
            // 
            this.labelDatumIzdavanja.AutoSize = true;
            this.labelDatumIzdavanja.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDatumIzdavanja.Location = new System.Drawing.Point(49, 194);
            this.labelDatumIzdavanja.Name = "labelDatumIzdavanja";
            this.labelDatumIzdavanja.Size = new System.Drawing.Size(175, 26);
            this.labelDatumIzdavanja.TabIndex = 37;
            this.labelDatumIzdavanja.Text = "Datum izdavanja";
            // 
            // btnSnimi
            // 
            this.btnSnimi.Location = new System.Drawing.Point(585, 374);
            this.btnSnimi.Name = "btnSnimi";
            this.btnSnimi.Size = new System.Drawing.Size(190, 45);
            this.btnSnimi.TabIndex = 35;
            this.btnSnimi.Text = "Snimi";
            this.btnSnimi.UseVisualStyleBackColor = true;
            this.btnSnimi.Click += new System.EventHandler(this.btnSnimi_Click);
            // 
            // txtTipRashoda
            // 
            this.txtTipRashoda.Location = new System.Drawing.Point(235, 29);
            this.txtTipRashoda.Name = "txtTipRashoda";
            this.txtTipRashoda.Size = new System.Drawing.Size(242, 26);
            this.txtTipRashoda.TabIndex = 34;
            // 
            // labelTipRashoda
            // 
            this.labelTipRashoda.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTipRashoda.Location = new System.Drawing.Point(49, 29);
            this.labelTipRashoda.Name = "labelTipRashoda";
            this.labelTipRashoda.Size = new System.Drawing.Size(150, 41);
            this.labelTipRashoda.TabIndex = 33;
            this.labelTipRashoda.Text = "Tip rashoda";
            // 
            // txtIzdavaoc
            // 
            this.txtIzdavaoc.Location = new System.Drawing.Point(235, 134);
            this.txtIzdavaoc.Name = "txtIzdavaoc";
            this.txtIzdavaoc.Size = new System.Drawing.Size(242, 26);
            this.txtIzdavaoc.TabIndex = 41;
            // 
            // labelIzdavaoc
            // 
            this.labelIzdavaoc.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelIzdavaoc.Location = new System.Drawing.Point(49, 132);
            this.labelIzdavaoc.Name = "labelIzdavaoc";
            this.labelIzdavaoc.Size = new System.Drawing.Size(150, 41);
            this.labelIzdavaoc.TabIndex = 40;
            this.labelIzdavaoc.Text = "Izdavaoc";
            // 
            // txtCena
            // 
            this.txtCena.Location = new System.Drawing.Point(235, 82);
            this.txtCena.Name = "txtCena";
            this.txtCena.Size = new System.Drawing.Size(242, 26);
            this.txtCena.TabIndex = 43;
            // 
            // labelCena
            // 
            this.labelCena.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCena.Location = new System.Drawing.Point(49, 82);
            this.labelCena.Name = "labelCena";
            this.labelCena.Size = new System.Drawing.Size(150, 41);
            this.labelCena.TabIndex = 42;
            this.labelCena.Text = "Cena";
            // 
            // DodajRashod
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1098, 560);
            this.Controls.Add(this.txtCena);
            this.Controls.Add(this.labelCena);
            this.Controls.Add(this.txtIzdavaoc);
            this.Controls.Add(this.labelIzdavaoc);
            this.Controls.Add(this.dtDatumIzdavanja);
            this.Controls.Add(this.labelDatumIzdavanja);
            this.Controls.Add(this.btnSnimi);
            this.Controls.Add(this.txtTipRashoda);
            this.Controls.Add(this.labelTipRashoda);
            this.Name = "DodajRashod";
            this.Text = "DodajRashod";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DateTimePicker dtDatumIzdavanja;
        private System.Windows.Forms.Label labelDatumIzdavanja;
        private System.Windows.Forms.Button btnSnimi;
        private System.Windows.Forms.TextBox txtTipRashoda;
        private System.Windows.Forms.Label labelTipRashoda;
        private System.Windows.Forms.TextBox txtIzdavaoc;
        private System.Windows.Forms.Label labelIzdavaoc;
        private System.Windows.Forms.TextBox txtCena;
        private System.Windows.Forms.Label labelCena;
    }
}