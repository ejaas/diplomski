﻿
namespace Diplomski.Forme
{
    partial class DodajClana
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtRod = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtImeRoditelja = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtPrezime = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtIme = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnSnimi = new System.Windows.Forms.Button();
            this.txtJmbg = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtTelefon = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtAdresa = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtBrLK = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.checkedListBoxTipClanarine = new System.Windows.Forms.CheckedListBox();
            this.btnDodajTipClanarine = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.txtTipClanarine = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.checkBoxClanarina = new System.Windows.Forms.CheckBox();
            this.txtCenaClanarine = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.labDatumPocetka = new System.Windows.Forms.Label();
            this.labDatumIsteka = new System.Windows.Forms.Label();
            this.dtDatumPocetka = new System.Windows.Forms.DateTimePicker();
            this.dtDatumIsteka = new System.Windows.Forms.DateTimePicker();
            this.SuspendLayout();
            // 
            // txtEmail
            // 
            this.txtEmail.Location = new System.Drawing.Point(255, 345);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(226, 26);
            this.txtEmail.TabIndex = 27;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(83, 345);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(48, 20);
            this.label7.TabIndex = 26;
            this.label7.Text = "Email";
            // 
            // txtRod
            // 
            this.txtRod.Location = new System.Drawing.Point(255, 203);
            this.txtRod.Name = "txtRod";
            this.txtRod.Size = new System.Drawing.Size(226, 26);
            this.txtRod.TabIndex = 25;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(86, 206);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(39, 20);
            this.label6.TabIndex = 24;
            this.label6.Text = "Rod";
            // 
            // txtImeRoditelja
            // 
            this.txtImeRoditelja.Location = new System.Drawing.Point(255, 275);
            this.txtImeRoditelja.Name = "txtImeRoditelja";
            this.txtImeRoditelja.Size = new System.Drawing.Size(226, 26);
            this.txtImeRoditelja.TabIndex = 23;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(83, 275);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(95, 20);
            this.label5.TabIndex = 22;
            this.label5.Text = "Ime roditelja";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(86, 145);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(66, 20);
            this.label4.TabIndex = 20;
            this.label4.Text = "Prezime";
            // 
            // txtPrezime
            // 
            this.txtPrezime.Location = new System.Drawing.Point(255, 145);
            this.txtPrezime.Name = "txtPrezime";
            this.txtPrezime.Size = new System.Drawing.Size(226, 26);
            this.txtPrezime.TabIndex = 19;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(86, 92);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(36, 20);
            this.label3.TabIndex = 18;
            this.label3.Text = "Ime";
            // 
            // txtIme
            // 
            this.txtIme.Location = new System.Drawing.Point(255, 92);
            this.txtIme.Name = "txtIme";
            this.txtIme.Size = new System.Drawing.Size(226, 26);
            this.txtIme.TabIndex = 17;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(363, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(599, 48);
            this.label1.TabIndex = 15;
            this.label1.Text = "Sva input polja moraju da imaju vrednost ";
            // 
            // btnSnimi
            // 
            this.btnSnimi.Location = new System.Drawing.Point(529, 602);
            this.btnSnimi.Name = "btnSnimi";
            this.btnSnimi.Size = new System.Drawing.Size(146, 51);
            this.btnSnimi.TabIndex = 14;
            this.btnSnimi.Text = "Snimi";
            this.btnSnimi.UseVisualStyleBackColor = true;
            this.btnSnimi.Click += new System.EventHandler(this.btnSnimi_Click);
            // 
            // txtJmbg
            // 
            this.txtJmbg.Location = new System.Drawing.Point(252, 540);
            this.txtJmbg.Name = "txtJmbg";
            this.txtJmbg.Size = new System.Drawing.Size(226, 26);
            this.txtJmbg.TabIndex = 33;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(83, 540);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(54, 20);
            this.label2.TabIndex = 32;
            this.label2.Text = "JMBG";
            // 
            // txtTelefon
            // 
            this.txtTelefon.Location = new System.Drawing.Point(252, 404);
            this.txtTelefon.Name = "txtTelefon";
            this.txtTelefon.Size = new System.Drawing.Size(226, 26);
            this.txtTelefon.TabIndex = 31;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(83, 407);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(62, 20);
            this.label8.TabIndex = 30;
            this.label8.Text = "Telefon";
            // 
            // txtAdresa
            // 
            this.txtAdresa.Location = new System.Drawing.Point(252, 476);
            this.txtAdresa.Name = "txtAdresa";
            this.txtAdresa.Size = new System.Drawing.Size(226, 26);
            this.txtAdresa.TabIndex = 29;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(80, 476);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(60, 20);
            this.label9.TabIndex = 28;
            this.label9.Text = "Adresa";
            // 
            // txtBrLK
            // 
            this.txtBrLK.Location = new System.Drawing.Point(252, 602);
            this.txtBrLK.Name = "txtBrLK";
            this.txtBrLK.Size = new System.Drawing.Size(226, 26);
            this.txtBrLK.TabIndex = 35;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(80, 602);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(105, 20);
            this.label10.TabIndex = 34;
            this.label10.Text = "Br. licne karte";
            // 
            // label11
            // 
            this.label11.Location = new System.Drawing.Point(635, 476);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(141, 89);
            this.label11.TabIndex = 37;
            this.label11.Text = "Izaberite tip clanarine za dodatog clana";
            this.label11.Visible = false;
            // 
            // checkedListBoxTipClanarine
            // 
            this.checkedListBoxTipClanarine.FormattingEnabled = true;
            this.checkedListBoxTipClanarine.Location = new System.Drawing.Point(782, 476);
            this.checkedListBoxTipClanarine.Name = "checkedListBoxTipClanarine";
            this.checkedListBoxTipClanarine.Size = new System.Drawing.Size(340, 96);
            this.checkedListBoxTipClanarine.TabIndex = 36;
            this.checkedListBoxTipClanarine.Visible = false;
            this.checkedListBoxTipClanarine.SelectedIndexChanged += new System.EventHandler(this.checkedListBoxTipClanarine_SelectedIndexChanged);
            // 
            // btnDodajTipClanarine
            // 
            this.btnDodajTipClanarine.Location = new System.Drawing.Point(1013, 269);
            this.btnDodajTipClanarine.Name = "btnDodajTipClanarine";
            this.btnDodajTipClanarine.Size = new System.Drawing.Size(92, 63);
            this.btnDodajTipClanarine.TabIndex = 40;
            this.btnDodajTipClanarine.Text = "Dodaj tip clanarine";
            this.btnDodajTipClanarine.UseVisualStyleBackColor = true;
            this.btnDodajTipClanarine.Visible = false;
            this.btnDodajTipClanarine.Click += new System.EventHandler(this.btnDodajTipClanarine_Click);
            // 
            // label12
            // 
            this.label12.Location = new System.Drawing.Point(652, 257);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(112, 46);
            this.label12.TabIndex = 39;
            this.label12.Text = "Dodajte novi tip clanarine";
            this.label12.Visible = false;
            // 
            // txtTipClanarine
            // 
            this.txtTipClanarine.Location = new System.Drawing.Point(793, 269);
            this.txtTipClanarine.Name = "txtTipClanarine";
            this.txtTipClanarine.Size = new System.Drawing.Size(179, 26);
            this.txtTipClanarine.TabIndex = 38;
            this.txtTipClanarine.Visible = false;
            this.txtTipClanarine.TextChanged += new System.EventHandler(this.txtTipClanarine_TextChanged);
            // 
            // label13
            // 
            this.label13.Location = new System.Drawing.Point(677, 82);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(128, 63);
            this.label13.TabIndex = 42;
            this.label13.Text = "Da li dodajete novom clanu clanarinu?";
            this.label13.Click += new System.EventHandler(this.label13_Click);
            // 
            // checkBoxClanarina
            // 
            this.checkBoxClanarina.AutoSize = true;
            this.checkBoxClanarina.Location = new System.Drawing.Point(823, 94);
            this.checkBoxClanarina.Name = "checkBoxClanarina";
            this.checkBoxClanarina.Size = new System.Drawing.Size(56, 24);
            this.checkBoxClanarina.TabIndex = 41;
            this.checkBoxClanarina.Text = "Da";
            this.checkBoxClanarina.UseVisualStyleBackColor = true;
            this.checkBoxClanarina.CheckedChanged += new System.EventHandler(this.checkBoxClanarina_CheckedChanged);
            // 
            // txtCenaClanarine
            // 
            this.txtCenaClanarine.Location = new System.Drawing.Point(793, 307);
            this.txtCenaClanarine.Name = "txtCenaClanarine";
            this.txtCenaClanarine.Size = new System.Drawing.Size(179, 26);
            this.txtCenaClanarine.TabIndex = 43;
            this.txtCenaClanarine.Visible = false;
            this.txtCenaClanarine.TextChanged += new System.EventHandler(this.txtCenaClanarine_TextChanged);
            // 
            // label14
            // 
            this.label14.Location = new System.Drawing.Point(652, 315);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(112, 18);
            this.label14.TabIndex = 44;
            this.label14.Text = "Njena cena";
            this.label14.Visible = false;
            // 
            // labDatumPocetka
            // 
            this.labDatumPocetka.Location = new System.Drawing.Point(652, 365);
            this.labDatumPocetka.Name = "labDatumPocetka";
            this.labDatumPocetka.Size = new System.Drawing.Size(134, 23);
            this.labDatumPocetka.TabIndex = 45;
            this.labDatumPocetka.Text = "Datum pocetka";
            this.labDatumPocetka.Visible = false;
            // 
            // labDatumIsteka
            // 
            this.labDatumIsteka.Location = new System.Drawing.Point(652, 404);
            this.labDatumIsteka.Name = "labDatumIsteka";
            this.labDatumIsteka.Size = new System.Drawing.Size(134, 23);
            this.labDatumIsteka.TabIndex = 46;
            this.labDatumIsteka.Text = "Datum isteka";
            this.labDatumIsteka.Visible = false;
            // 
            // dtDatumPocetka
            // 
            this.dtDatumPocetka.Location = new System.Drawing.Point(792, 360);
            this.dtDatumPocetka.Name = "dtDatumPocetka";
            this.dtDatumPocetka.Size = new System.Drawing.Size(200, 26);
            this.dtDatumPocetka.TabIndex = 47;
            // 
            // dtDatumIsteka
            // 
            this.dtDatumIsteka.Location = new System.Drawing.Point(793, 407);
            this.dtDatumIsteka.Name = "dtDatumIsteka";
            this.dtDatumIsteka.Size = new System.Drawing.Size(200, 26);
            this.dtDatumIsteka.TabIndex = 48;
            // 
            // DodajClana
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1170, 682);
            this.Controls.Add(this.dtDatumIsteka);
            this.Controls.Add(this.dtDatumPocetka);
            this.Controls.Add(this.labDatumIsteka);
            this.Controls.Add(this.labDatumPocetka);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.txtCenaClanarine);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.checkBoxClanarina);
            this.Controls.Add(this.btnDodajTipClanarine);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.txtTipClanarine);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.checkedListBoxTipClanarine);
            this.Controls.Add(this.txtBrLK);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.txtJmbg);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtTelefon);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.txtAdresa);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.txtEmail);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtRod);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtImeRoditelja);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtPrezime);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtIme);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnSnimi);
            this.Name = "DodajClana";
            this.Text = "DodajClana";
            this.Load += new System.EventHandler(this.DodajClana_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtRod;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtImeRoditelja;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtPrezime;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtIme;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnSnimi;
        private System.Windows.Forms.TextBox txtJmbg;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtTelefon;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtAdresa;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtBrLK;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.CheckedListBox checkedListBoxTipClanarine;
        private System.Windows.Forms.Button btnDodajTipClanarine;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtTipClanarine;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.CheckBox checkBoxClanarina;
        private System.Windows.Forms.TextBox txtCenaClanarine;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label labDatumPocetka;
        private System.Windows.Forms.Label labDatumIsteka;
        private System.Windows.Forms.DateTimePicker dtDatumPocetka;
        private System.Windows.Forms.DateTimePicker dtDatumIsteka;
    }
}