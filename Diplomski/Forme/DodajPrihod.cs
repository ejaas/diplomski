﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Diplomski.Forme
{
    public partial class DodajPrihod : Form
    {
        public DodajPrihod()
        {
            InitializeComponent();
        }

        

        private void btnDodajClanarinu_Click(object sender, EventArgs e)
        {
            DodajClanarinu forma = new DodajClanarinu();
            this.Visible = false;
            forma.ShowDialog();
            this.Visible = true;
            this.DialogResult = DialogResult.OK;
        }

        private void btnDodajUslugu_Click(object sender, EventArgs e)
        {
            DodajUslugu forma = new DodajUslugu();
            this.Visible = false;
            forma.ShowDialog();
            this.Visible = true;
            this.DialogResult = DialogResult.OK;
        }

        private void btnDodajPublikaciju_Click(object sender, EventArgs e)
        {
            DodajPublikaciju forma = new DodajPublikaciju();
            this.Visible = false;
            forma.ShowDialog();
            this.Visible = true;
            this.DialogResult = DialogResult.OK;
        }

        private void btnDodajDonaciju_Click(object sender, EventArgs e)
        {
            DodajDonaciju forma = new DodajDonaciju();
            this.Visible = false;
            forma.ShowDialog();
            this.Visible = true;
            this.DialogResult = DialogResult.OK;
        }
    }
}
