﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Diplomski.Entiteti;
using NHibernate;
using System.Windows.Forms;

namespace Diplomski.Forme
{
    public partial class DodajUslugu : Form
    {
        public DodajUslugu()
        {
            InitializeComponent();
        }

        private void btnSnimi_Click(object sender, EventArgs e)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                foreach (string i in listBox1.Items)
                {
                    Prihod.Usluga p = new Prihod.Usluga()
                    {

                        Tip_Usluge = i
                    };

                    s.Save(p);
                    s.Flush();
                }

                s.Close();
                this.DialogResult = DialogResult.OK;
            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.ToString());
            }
        }

        private void btnDodajPublikaciju_Click(object sender, EventArgs e)
        {
            listBox1.Items.Add(txtUsluga.Text);
            txtUsluga.Text = "";
        }
    }
}
