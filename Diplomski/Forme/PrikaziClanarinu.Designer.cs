﻿
namespace Diplomski.Forme
{
    partial class PrikaziClanarinu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listView2 = new System.Windows.Forms.ListView();
            this.columnIdPrihoda = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnTipClanarine = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnCenaClanarine = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnDatumPocetka = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnDatumIsteka = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.labelDatumPocetka = new System.Windows.Forms.Label();
            this.labelDatumIsteka = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.dtDatumPocetka = new System.Windows.Forms.DateTimePicker();
            this.dtDatumIsteka = new System.Windows.Forms.DateTimePicker();
            this.txtVremeVazenja = new System.Windows.Forms.TextBox();
            this.btnPrikaziBrojPreostalihDana = new System.Windows.Forms.Button();
            this.txtBrojPreostalihDana = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // listView2
            // 
            this.listView2.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnIdPrihoda,
            this.columnTipClanarine,
            this.columnCenaClanarine,
            this.columnDatumPocetka,
            this.columnDatumIsteka});
            this.listView2.HideSelection = false;
            this.listView2.Location = new System.Drawing.Point(31, 78);
            this.listView2.Name = "listView2";
            this.listView2.Size = new System.Drawing.Size(1154, 267);
            this.listView2.TabIndex = 75;
            this.listView2.UseCompatibleStateImageBehavior = false;
            this.listView2.View = System.Windows.Forms.View.Details;
            this.listView2.SelectedIndexChanged += new System.EventHandler(this.listView2_SelectedIndexChanged);
            // 
            // columnIdPrihoda
            // 
            this.columnIdPrihoda.Text = "IdPrihoda";
            this.columnIdPrihoda.Width = 151;
            // 
            // columnTipClanarine
            // 
            this.columnTipClanarine.Text = "Tip Clanarine";
            this.columnTipClanarine.Width = 143;
            // 
            // columnCenaClanarine
            // 
            this.columnCenaClanarine.Text = "Cena Clanarine";
            this.columnCenaClanarine.Width = 217;
            // 
            // columnDatumPocetka
            // 
            this.columnDatumPocetka.Text = "Datum Pocetka";
            this.columnDatumPocetka.Width = 161;
            // 
            // columnDatumIsteka
            // 
            this.columnDatumIsteka.Text = "Datum Isteka";
            this.columnDatumIsteka.Width = 206;
            // 
            // labelDatumPocetka
            // 
            this.labelDatumPocetka.AutoSize = true;
            this.labelDatumPocetka.Location = new System.Drawing.Point(67, 387);
            this.labelDatumPocetka.Name = "labelDatumPocetka";
            this.labelDatumPocetka.Size = new System.Drawing.Size(118, 20);
            this.labelDatumPocetka.TabIndex = 76;
            this.labelDatumPocetka.Text = "Datum pocetka";
            // 
            // labelDatumIsteka
            // 
            this.labelDatumIsteka.AutoSize = true;
            this.labelDatumIsteka.Location = new System.Drawing.Point(67, 449);
            this.labelDatumIsteka.Name = "labelDatumIsteka";
            this.labelDatumIsteka.Size = new System.Drawing.Size(105, 20);
            this.labelDatumIsteka.TabIndex = 78;
            this.labelDatumIsteka.Text = "Datum Isteka";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(517, 387);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(148, 82);
            this.button2.TabIndex = 81;
            this.button2.Text = "Prikazi broj dana vazenja odabrane članarine ";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // dtDatumPocetka
            // 
            this.dtDatumPocetka.Location = new System.Drawing.Point(223, 387);
            this.dtDatumPocetka.Name = "dtDatumPocetka";
            this.dtDatumPocetka.Size = new System.Drawing.Size(200, 26);
            this.dtDatumPocetka.TabIndex = 82;
            // 
            // dtDatumIsteka
            // 
            this.dtDatumIsteka.Location = new System.Drawing.Point(223, 444);
            this.dtDatumIsteka.Name = "dtDatumIsteka";
            this.dtDatumIsteka.Size = new System.Drawing.Size(200, 26);
            this.dtDatumIsteka.TabIndex = 83;
            // 
            // txtVremeVazenja
            // 
            this.txtVremeVazenja.Location = new System.Drawing.Point(749, 415);
            this.txtVremeVazenja.Name = "txtVremeVazenja";
            this.txtVremeVazenja.Size = new System.Drawing.Size(152, 26);
            this.txtVremeVazenja.TabIndex = 84;
            this.txtVremeVazenja.TextChanged += new System.EventHandler(this.txtVremeVazenja_TextChanged);
            // 
            // btnPrikaziBrojPreostalihDana
            // 
            this.btnPrikaziBrojPreostalihDana.Location = new System.Drawing.Point(517, 480);
            this.btnPrikaziBrojPreostalihDana.Name = "btnPrikaziBrojPreostalihDana";
            this.btnPrikaziBrojPreostalihDana.Size = new System.Drawing.Size(148, 87);
            this.btnPrikaziBrojPreostalihDana.TabIndex = 85;
            this.btnPrikaziBrojPreostalihDana.Text = "Prikaži broj dana od današnjeg isteka članarina";
            this.btnPrikaziBrojPreostalihDana.UseVisualStyleBackColor = true;
            this.btnPrikaziBrojPreostalihDana.Click += new System.EventHandler(this.btnPrikaziBrojPreostalihDana_Click);
            // 
            // txtBrojPreostalihDana
            // 
            this.txtBrojPreostalihDana.Location = new System.Drawing.Point(749, 510);
            this.txtBrojPreostalihDana.Name = "txtBrojPreostalihDana";
            this.txtBrojPreostalihDana.Size = new System.Drawing.Size(152, 26);
            this.txtBrojPreostalihDana.TabIndex = 86;
            // 
            // PrikaziClanarinu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1241, 667);
            this.Controls.Add(this.txtBrojPreostalihDana);
            this.Controls.Add(this.btnPrikaziBrojPreostalihDana);
            this.Controls.Add(this.txtVremeVazenja);
            this.Controls.Add(this.dtDatumIsteka);
            this.Controls.Add(this.dtDatumPocetka);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.labelDatumIsteka);
            this.Controls.Add(this.labelDatumPocetka);
            this.Controls.Add(this.listView2);
            this.Name = "PrikaziClanarinu";
            this.Text = "PrikaziClanarinu";
            this.Load += new System.EventHandler(this.ModifikujClanarinu_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView listView2;
        private System.Windows.Forms.Label labelDatumPocetka;
        private System.Windows.Forms.ColumnHeader columnIdPrihoda;
        private System.Windows.Forms.ColumnHeader columnTipClanarine;
        private System.Windows.Forms.ColumnHeader columnCenaClanarine;
        private System.Windows.Forms.ColumnHeader columnDatumPocetka;
        private System.Windows.Forms.ColumnHeader columnDatumIsteka;
        private System.Windows.Forms.Label labelDatumIsteka;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.DateTimePicker dtDatumPocetka;
        private System.Windows.Forms.DateTimePicker dtDatumIsteka;
        private System.Windows.Forms.TextBox txtVremeVazenja;
        private System.Windows.Forms.Button btnPrikaziBrojPreostalihDana;
        private System.Windows.Forms.TextBox txtBrojPreostalihDana;
    }
}