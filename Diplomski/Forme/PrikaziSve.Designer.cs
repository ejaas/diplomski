﻿
namespace Diplomski.Forme
{
    partial class PrikaziSve
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listView2 = new System.Windows.Forms.ListView();
            this.columnIdIma = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnBrClanarine = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnBrRacuna = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnBrClan = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.txtBrojRacuna = new System.Windows.Forms.TextBox();
            this.lblBrojRacuna = new System.Windows.Forms.Label();
            this.listView1 = new System.Windows.Forms.ListView();
            this.columnIDCLAN = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnIme = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnPrezime = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnEmail = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnTelefon = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnAdresa = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnRod = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnJmbg = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnBrLk = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnImeroditelja = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.txtBrojClana = new System.Windows.Forms.TextBox();
            this.lblBrojClana = new System.Windows.Forms.Label();
            this.btnDodajClanaClanarini = new System.Windows.Forms.Button();
            this.listView4 = new System.Windows.Forms.ListView();
            this.columnIDCLANARINE = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnTipClanarine = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnCenaClanarine = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnDatumPocetka = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnDatumIsteka = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.label1 = new System.Windows.Forms.Label();
            this.txtDodajBrojRacuna = new System.Windows.Forms.TextBox();
            this.txtIdClanarine = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.lblPlatio = new System.Windows.Forms.Label();
            this.txtUplata = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtIme = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.txtString = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // listView2
            // 
            this.listView2.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnIdIma,
            this.columnBrClanarine,
            this.columnBrRacuna,
            this.columnBrClan});
            this.listView2.HideSelection = false;
            this.listView2.Location = new System.Drawing.Point(12, 21);
            this.listView2.Name = "listView2";
            this.listView2.Size = new System.Drawing.Size(756, 267);
            this.listView2.TabIndex = 76;
            this.listView2.UseCompatibleStateImageBehavior = false;
            this.listView2.View = System.Windows.Forms.View.Details;
            this.listView2.SelectedIndexChanged += new System.EventHandler(this.listView2_SelectedIndexChanged);
            // 
            // columnIdIma
            // 
            this.columnIdIma.Text = "Id ";
            this.columnIdIma.Width = 109;
            // 
            // columnBrClanarine
            // 
            this.columnBrClanarine.Text = "Broj Clanarine";
            this.columnBrClanarine.Width = 150;
            // 
            // columnBrRacuna
            // 
            this.columnBrRacuna.Text = "Broj Racuna";
            this.columnBrRacuna.Width = 155;
            // 
            // columnBrClan
            // 
            this.columnBrClan.Text = "Broj Clana";
            this.columnBrClan.Width = 124;
            // 
            // txtBrojRacuna
            // 
            this.txtBrojRacuna.Location = new System.Drawing.Point(37, 374);
            this.txtBrojRacuna.Name = "txtBrojRacuna";
            this.txtBrojRacuna.Size = new System.Drawing.Size(156, 26);
            this.txtBrojRacuna.TabIndex = 78;
            this.txtBrojRacuna.TextChanged += new System.EventHandler(this.txtBrojRacuna_TextChanged);
            // 
            // lblBrojRacuna
            // 
            this.lblBrojRacuna.AutoSize = true;
            this.lblBrojRacuna.Location = new System.Drawing.Point(33, 338);
            this.lblBrojRacuna.Name = "lblBrojRacuna";
            this.lblBrojRacuna.Size = new System.Drawing.Size(160, 20);
            this.lblBrojRacuna.TabIndex = 77;
            this.lblBrojRacuna.Text = "Pretrazi po br. racuna";
            // 
            // listView1
            // 
            this.listView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnIDCLAN,
            this.columnIme,
            this.columnPrezime,
            this.columnEmail,
            this.columnTelefon,
            this.columnAdresa,
            this.columnRod,
            this.columnJmbg,
            this.columnBrLk,
            this.columnImeroditelja});
            this.listView1.HideSelection = false;
            this.listView1.Location = new System.Drawing.Point(819, 12);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(423, 218);
            this.listView1.TabIndex = 79;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.Details;
            this.listView1.SelectedIndexChanged += new System.EventHandler(this.listView1_SelectedIndexChanged);
            // 
            // columnIDCLAN
            // 
            this.columnIDCLAN.Text = "Id";
            // 
            // columnIme
            // 
            this.columnIme.Text = "Ime";
            this.columnIme.Width = 95;
            // 
            // columnPrezime
            // 
            this.columnPrezime.Text = "Prezime";
            this.columnPrezime.Width = 117;
            // 
            // columnEmail
            // 
            this.columnEmail.Text = "Email";
            this.columnEmail.Width = 141;
            // 
            // columnTelefon
            // 
            this.columnTelefon.Text = "Telefon";
            this.columnTelefon.Width = 136;
            // 
            // columnAdresa
            // 
            this.columnAdresa.Text = "Adresa";
            this.columnAdresa.Width = 119;
            // 
            // columnRod
            // 
            this.columnRod.Text = "Rod";
            this.columnRod.Width = 81;
            // 
            // columnJmbg
            // 
            this.columnJmbg.Text = "Jmbg";
            this.columnJmbg.Width = 89;
            // 
            // columnBrLk
            // 
            this.columnBrLk.Text = "BrLK";
            this.columnBrLk.Width = 86;
            // 
            // columnImeroditelja
            // 
            this.columnImeroditelja.Text = "Roditelj";
            // 
            // txtBrojClana
            // 
            this.txtBrojClana.Location = new System.Drawing.Point(938, 236);
            this.txtBrojClana.Name = "txtBrojClana";
            this.txtBrojClana.Size = new System.Drawing.Size(226, 26);
            this.txtBrojClana.TabIndex = 82;
            this.txtBrojClana.TextChanged += new System.EventHandler(this.txtBrojClana_TextChanged);
            // 
            // lblBrojClana
            // 
            this.lblBrojClana.AutoSize = true;
            this.lblBrojClana.Location = new System.Drawing.Point(815, 242);
            this.lblBrojClana.Name = "lblBrojClana";
            this.lblBrojClana.Size = new System.Drawing.Size(102, 20);
            this.lblBrojClana.TabIndex = 81;
            this.lblBrojClana.Text = "Izaberi Clana";
            // 
            // btnDodajClanaClanarini
            // 
            this.btnDodajClanaClanarini.Location = new System.Drawing.Point(1258, 212);
            this.btnDodajClanaClanarini.Name = "btnDodajClanaClanarini";
            this.btnDodajClanaClanarini.Size = new System.Drawing.Size(181, 80);
            this.btnDodajClanaClanarini.TabIndex = 86;
            this.btnDodajClanaClanarini.Text = "Dodaj Clanarinu Clanu";
            this.btnDodajClanaClanarini.UseVisualStyleBackColor = true;
            this.btnDodajClanaClanarini.Click += new System.EventHandler(this.btnDodajClanaClanarini_Click);
            // 
            // listView4
            // 
            this.listView4.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnIDCLANARINE,
            this.columnTipClanarine,
            this.columnCenaClanarine,
            this.columnDatumPocetka,
            this.columnDatumIsteka});
            this.listView4.HideSelection = false;
            this.listView4.Location = new System.Drawing.Point(810, 302);
            this.listView4.Name = "listView4";
            this.listView4.Size = new System.Drawing.Size(432, 230);
            this.listView4.TabIndex = 97;
            this.listView4.UseCompatibleStateImageBehavior = false;
            this.listView4.View = System.Windows.Forms.View.Details;
            this.listView4.SelectedIndexChanged += new System.EventHandler(this.listView4_SelectedIndexChanged);
            // 
            // columnIDCLANARINE
            // 
            this.columnIDCLANARINE.Text = "Id";
            this.columnIDCLANARINE.Width = 151;
            // 
            // columnTipClanarine
            // 
            this.columnTipClanarine.Text = "Tip Clanarine";
            this.columnTipClanarine.Width = 143;
            // 
            // columnCenaClanarine
            // 
            this.columnCenaClanarine.Text = "Cena Clanarine";
            this.columnCenaClanarine.Width = 217;
            // 
            // columnDatumPocetka
            // 
            this.columnDatumPocetka.Text = "Datum Pocetka";
            this.columnDatumPocetka.Width = 161;
            // 
            // columnDatumIsteka
            // 
            this.columnDatumIsteka.Text = "Datum Isteka";
            this.columnDatumIsteka.Width = 206;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(741, 605);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(151, 20);
            this.label1.TabIndex = 99;
            this.label1.Text = "Broj racuna ugovora";
            // 
            // txtDodajBrojRacuna
            // 
            this.txtDodajBrojRacuna.Location = new System.Drawing.Point(904, 602);
            this.txtDodajBrojRacuna.Name = "txtDodajBrojRacuna";
            this.txtDodajBrojRacuna.Size = new System.Drawing.Size(72, 26);
            this.txtDodajBrojRacuna.TabIndex = 98;
            // 
            // txtIdClanarine
            // 
            this.txtIdClanarine.Location = new System.Drawing.Point(956, 552);
            this.txtIdClanarine.Name = "txtIdClanarine";
            this.txtIdClanarine.Size = new System.Drawing.Size(226, 26);
            this.txtIdClanarine.TabIndex = 100;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(806, 558);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(128, 20);
            this.label2.TabIndex = 101;
            this.label2.Text = "Izaberi Clanarinu";
            // 
            // lblPlatio
            // 
            this.lblPlatio.AutoSize = true;
            this.lblPlatio.Location = new System.Drawing.Point(1043, 605);
            this.lblPlatio.Name = "lblPlatio";
            this.lblPlatio.Size = new System.Drawing.Size(60, 20);
            this.lblPlatio.TabIndex = 103;
            this.lblPlatio.Text = "Uplata:";
            // 
            // txtUplata
            // 
            this.txtUplata.Location = new System.Drawing.Point(1121, 602);
            this.txtUplata.Name = "txtUplata";
            this.txtUplata.Size = new System.Drawing.Size(97, 26);
            this.txtUplata.TabIndex = 102;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 619);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(382, 20);
            this.label3.TabIndex = 104;
            this.label3.Text = "Klikom na Id člana, članarine ili računa birate iz tabela";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(815, 272);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(45, 20);
            this.label4.TabIndex = 105;
            this.label4.Text = "Clan:";
            // 
            // txtIme
            // 
            this.txtIme.Location = new System.Drawing.Point(938, 270);
            this.txtIme.Name = "txtIme";
            this.txtIme.Size = new System.Drawing.Size(226, 26);
            this.txtIme.TabIndex = 106;
            this.txtIme.TextChanged += new System.EventHandler(this.txtIme_TextChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(1273, 38);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(127, 20);
            this.label5.TabIndex = 110;
            this.label5.Text = "Za bržu pretragu";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(1380, 97);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(148, 35);
            this.button1.TabIndex = 109;
            this.button1.Text = "Pretraži";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(1266, 58);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(262, 20);
            this.label6.TabIndex = 108;
            this.label6.Text = "Izaberite pocetno slovo imena člana";
            // 
            // txtString
            // 
            this.txtString.Location = new System.Drawing.Point(1277, 106);
            this.txtString.Name = "txtString";
            this.txtString.Size = new System.Drawing.Size(63, 26);
            this.txtString.TabIndex = 107;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(217, 338);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(148, 48);
            this.button2.TabIndex = 111;
            this.button2.Text = "Pretraži";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // PrikaziSve
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1564, 661);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtString);
            this.Controls.Add(this.txtIme);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lblPlatio);
            this.Controls.Add(this.txtUplata);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtIdClanarine);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtDodajBrojRacuna);
            this.Controls.Add(this.listView4);
            this.Controls.Add(this.btnDodajClanaClanarini);
            this.Controls.Add(this.txtBrojClana);
            this.Controls.Add(this.lblBrojClana);
            this.Controls.Add(this.listView1);
            this.Controls.Add(this.txtBrojRacuna);
            this.Controls.Add(this.lblBrojRacuna);
            this.Controls.Add(this.listView2);
            this.Name = "PrikaziSve";
            this.Text = "PrikaziSve";
            this.Load += new System.EventHandler(this.PrikaziSve_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView listView2;
        private System.Windows.Forms.ColumnHeader columnBrClan;
        private System.Windows.Forms.ColumnHeader columnBrClanarine;
        private System.Windows.Forms.ColumnHeader columnBrRacuna;
        private System.Windows.Forms.ColumnHeader columnIdIma;
        private System.Windows.Forms.TextBox txtBrojRacuna;
        private System.Windows.Forms.Label lblBrojRacuna;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.ColumnHeader columnIDCLAN;
        private System.Windows.Forms.ColumnHeader columnIme;
        private System.Windows.Forms.ColumnHeader columnPrezime;
        private System.Windows.Forms.ColumnHeader columnEmail;
        private System.Windows.Forms.ColumnHeader columnTelefon;
        private System.Windows.Forms.ColumnHeader columnAdresa;
        private System.Windows.Forms.ColumnHeader columnRod;
        private System.Windows.Forms.ColumnHeader columnJmbg;
        private System.Windows.Forms.ColumnHeader columnBrLk;
        private System.Windows.Forms.ColumnHeader columnImeroditelja;
        private System.Windows.Forms.TextBox txtBrojClana;
        private System.Windows.Forms.Label lblBrojClana;
        private System.Windows.Forms.Button btnDodajClanaClanarini;
        private System.Windows.Forms.ListView listView4;
        private System.Windows.Forms.ColumnHeader columnIDCLANARINE;
        private System.Windows.Forms.ColumnHeader columnTipClanarine;
        private System.Windows.Forms.ColumnHeader columnCenaClanarine;
        private System.Windows.Forms.ColumnHeader columnDatumPocetka;
        private System.Windows.Forms.ColumnHeader columnDatumIsteka;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtDodajBrojRacuna;
        private System.Windows.Forms.TextBox txtIdClanarine;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblPlatio;
        private System.Windows.Forms.TextBox txtUplata;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtIme;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtString;
        private System.Windows.Forms.Button button2;
    }
}