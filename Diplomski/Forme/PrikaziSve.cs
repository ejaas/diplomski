﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Diplomski.Entiteti;
using NHibernate;
using static Diplomski.DTOs;

namespace Diplomski.Forme
{
    public partial class PrikaziSve : Form
    {
        private Ima ima;
        private Clan cl;
        private Prihod.Clanarina pr;
        private Uplata up;
        public PrikaziSve()
        {
            InitializeComponent();
            ima = null;
            cl = null;
            pr = null;
            up = null;
        }

        private void listView2_SelectedIndexChanged(object sender, EventArgs e)
        {

            try
            {
                if (listView2.SelectedItems.Count > 0)
                {
                    ISession s = DataLayer.GetSession();

                    ima = s.Load<Ima>(int.Parse(listView2.SelectedItems[0].Text));

                    txtBrojRacuna.Text = ima.BrRacuna.ToString();

                    if (int.Parse(txtUplata.Text) > 0)
                    {
                        MessageBox.Show("Korisnik je uplatio pun iznos članarine");
                    }
                    else
                    {
                        MessageBox.Show("Korisnik nije uplatio članarinu");
                    }
                    

                    s.Close();
                }
            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.ToString());
            }
        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (listView1.SelectedItems.Count > 0)
                {
                    ISession s = DataLayer.GetSession();

                    cl = s.Load<Clan>(int.Parse(listView1.SelectedItems[0].Text));
                    txtBrojClana.Text = cl.IdClana.ToString();
                    txtIme.Text = cl.Ime + " " + cl.Prezime;
                    

                    s.Close();
                }
            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.ToString());
            }
        }

        private void listView4_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (listView4.SelectedItems.Count > 0)
                {
                    ISession s = DataLayer.GetSession();

                    pr = s.Load<Prihod.Clanarina>(int.Parse(listView4.SelectedItems[0].Text));
                    txtIdClanarine.Text = pr.IdPrihoda.ToString();

                  

                    s.Close();
                }
            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.ToString());
            }
        }



        private void PopuniListuPrihoda()
        {
            IList<ClanarinaInfo> clanarina = DTOManager.VratiClanarine();

            foreach (ClanarinaInfo n in clanarina)
            {
                ListViewItem item = new ListViewItem(new string[] { n.IdPrihoda.ToString(), n.Tip_Clanarine, n.Cena_Clanarine.ToString(), n.Datum_Pocetka.ToString(), n.Datum_Isteka.ToString() });

                listView4.Items.Add(item);
            }
            listView4.Refresh();
        }

        private void PopuniListuClanova()
        {
            IList<ClanInfo> clanovi = DTOManager.VratiClanove();


            foreach (ClanInfo n in clanovi)
            {
                ListViewItem item = new ListViewItem(new string[] { n.IdClan.ToString(), n.Ime, n.Prezime, n.Email, n.Telefon.ToString(), n.Adresa, n.Rod, n.Jmbg.ToString(), n.brLk.ToString(), n.Imeroditelja });

                listView1.Items.Add(item);
            }
            listView1.Refresh();
        }
        private void PopuniListu()
        {
            IList<ImaInfo> ima = DTOManager.VratiVeza();

            foreach (ImaInfo n in ima)
            {
                ListViewItem item = new ListViewItem(new string[] { n.Id_Ima.ToString() , n.Br_Clanarine.ToString(), n.Br_Racuna.ToString(), n.Br_Clan.ToString()  });

                listView2.Items.Add(item);
            }

            listView2.Refresh();
        }
       

        private void txtBrojRacuna_TextChanged(object sender, EventArgs e)
        {

        }

        

        private void PrikaziSve_Load(object sender, EventArgs e)
        {
            try
            {
                this.PopuniListu();
                this.PopuniListuClanova();
                this.PopuniListuPrihoda();
                //this.PopuniListuTipovaClanarina();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void btnDodajClanaClanarini_Click(object sender, EventArgs e)
        {
            try
            {
                ISession s = DataLayer.GetSession();
                Ima im = new Ima()
                {
                    BrClana = int.Parse(txtBrojClana.Text),
                    BrClanarine = int.Parse(txtIdClanarine.Text),
                    BrRacuna = int.Parse(txtDodajBrojRacuna.Text)
                };

                s.Save(im);
                s.Flush();


                Uplata up = new Uplata()
                {
                    
                    Cena= float.Parse(txtUplata.Text),
    
                    Ime_Clan = txtIme.Text

                };
                s.Save(up);
                s.Flush(); 

                s.Close();
                //this.DialogResult = DialogResult.OK;

                listView2.Items.Clear();
                this.PopuniListu();
                this.txtIdClanarine.Clear();
                this.txtBrojClana.Clear();
                this.txtBrojRacuna.Clear();


            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.ToString());
            }
        }

        private void checkedListBoxTipClanarine_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

       // private void checkBoxClanarina_CheckedChanged(object sender, EventArgs e)
      //  {
        //    if (checkBoxClanarina.Checked)
       //     {
       //         lblIzaberiTip.Visible = true;
       //         checkedListBoxTipClanarine.Visible = true;
       //
        //    }
        //    else
       //     {
        //        lblIzaberiTip.Visible = false;
       //         checkedListBoxTipClanarine.Visible = false;
        //    }
       // } 

        private void listView3_SelectedIndexChanged_1(object sender, EventArgs e)
        {

        }

        private void txtBrojClana_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                IQuery q = s.CreateQuery("from Clan");

                // IEnumerable<Clan> clanovi = q.Enumerable<Clan>();
                IList<ClanInfo> clanovi = DTOManager.VratiClanove();

                listView1.Items.Clear();

                foreach (ClanInfo n in clanovi)
                {
                    if (n.Ime.Substring(0, 1) == txtString.Text)
                    {
                        listView1.Refresh();
                        ListViewItem item = new ListViewItem(new string[] { n.IdClan.ToString(), n.Ime, n.Prezime, n.Email, n.Telefon.ToString(), n.Adresa, n.Rod, n.Jmbg.ToString(), n.brLk.ToString(), n.Imeroditelja });

                        listView1.Items.Add(item);
                    }
                }
                listView1.Refresh();



                s.Close();

            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                IQuery q = s.CreateQuery("from Clan");

                // IEnumerable<Clan> clanovi = q.Enumerable<Clan>();
                IList<ImaInfo> ima = DTOManager.VratiVeza();

                listView2.Items.Clear();

                foreach (ImaInfo n in ima)
                {
                    if (n.Br_Racuna == int.Parse(txtBrojRacuna.Text))
                    {
                        listView2.Refresh();
                        ListViewItem item = new ListViewItem(new string[] { n.Id_Ima.ToString(), n.Br_Clanarine.ToString(), n.Br_Racuna.ToString(), n.Br_Clan.ToString() });

                        listView2.Items.Add(item);
                    }
                }
                listView2.Refresh();



                s.Close();

            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }
        }

        private void txtIme_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
