﻿
namespace Diplomski.Forme
{
    partial class DodajDonaciju
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnDodajDonaciju = new System.Windows.Forms.Button();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.txtDonacija = new System.Windows.Forms.TextBox();
            this.btnSnimi = new System.Windows.Forms.Button();
            this.labelTipDonacije = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnDodajDonaciju
            // 
            this.btnDodajDonaciju.Location = new System.Drawing.Point(485, 25);
            this.btnDodajDonaciju.Name = "btnDodajDonaciju";
            this.btnDodajDonaciju.Size = new System.Drawing.Size(120, 52);
            this.btnDodajDonaciju.TabIndex = 48;
            this.btnDodajDonaciju.Text = "Dodaj Donacije";
            this.btnDodajDonaciju.UseVisualStyleBackColor = true;
            this.btnDodajDonaciju.Click += new System.EventHandler(this.btnDodajDonaciju_Click);
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.ItemHeight = 20;
            this.listBox1.Location = new System.Drawing.Point(218, 99);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(242, 124);
            this.listBox1.TabIndex = 47;
            // 
            // txtDonacija
            // 
            this.txtDonacija.Location = new System.Drawing.Point(218, 37);
            this.txtDonacija.Name = "txtDonacija";
            this.txtDonacija.Size = new System.Drawing.Size(242, 26);
            this.txtDonacija.TabIndex = 46;
            // 
            // btnSnimi
            // 
            this.btnSnimi.Location = new System.Drawing.Point(572, 339);
            this.btnSnimi.Name = "btnSnimi";
            this.btnSnimi.Size = new System.Drawing.Size(190, 45);
            this.btnSnimi.TabIndex = 45;
            this.btnSnimi.Text = "Snimi";
            this.btnSnimi.UseVisualStyleBackColor = true;
            this.btnSnimi.Click += new System.EventHandler(this.btnSnimi_Click);
            // 
            // labelTipDonacije
            // 
            this.labelTipDonacije.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTipDonacije.Location = new System.Drawing.Point(27, 35);
            this.labelTipDonacije.Name = "labelTipDonacije";
            this.labelTipDonacije.Size = new System.Drawing.Size(150, 97);
            this.labelTipDonacije.TabIndex = 44;
            this.labelTipDonacije.Text = "Tip donacije";
            // 
            // DodajDonaciju
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.btnDodajDonaciju);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.txtDonacija);
            this.Controls.Add(this.btnSnimi);
            this.Controls.Add(this.labelTipDonacije);
            this.Name = "DodajDonaciju";
            this.Text = "DodajDonaciju";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnDodajDonaciju;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.TextBox txtDonacija;
        private System.Windows.Forms.Button btnSnimi;
        private System.Windows.Forms.Label labelTipDonacije;
    }
}