﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NHibernate;
using Diplomski.Entiteti;
using System.Globalization;

namespace Diplomski.Forme
{
    public partial class DodajClana : Form
    {
        public DodajClana()
        {
            InitializeComponent();
        }

        private void DodajClana_Load(object sender, EventArgs e)
        {
            this.PopuniListuTipovaClanarina();
          
        }
        public void PopuniListuTipovaClanarina()
        {
            try
            {
                

                ISession s = DataLayer.GetSession();

                IList<Prihod.Clanarina> tipClanarine = s.QueryOver<Prihod.Clanarina>()
                                              .List<Prihod.Clanarina>();

                checkedListBoxTipClanarine.Items.Clear();

                foreach (Prihod.Clanarina t in tipClanarine)
                {
                    checkedListBoxTipClanarine.Items.Add("Tip: " + t.Tip_Clanarine + " , " + " Cena: " + t.Cena_Clanarine);
                }
                checkedListBoxTipClanarine.Refresh();


                s.Close();

            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.ToString());
            }
        }
        private void btnSnimi_Click(object sender, EventArgs e)
        {
          
            try
            {
                ISession s = DataLayer.GetSession();
                Clan p = new Clan()
                {
                    Ime = txtIme.Text,
                    Prezime = txtPrezime.Text,
                    Rod=txtRod.Text,
                    ImeRoditelja = txtImeRoditelja.Text,
                    Email = txtEmail.Text,
                    Telefon = int.Parse(txtTelefon.Text),
                    Adresa = txtAdresa.Text,
                    Jmbg= int.Parse(txtJmbg.Text),
                    BrLicneKarte= int.Parse(txtBrLK.Text)
                    
                };

                s.Save(p);
                s.Flush();

                s.Close();
                this.DialogResult = DialogResult.OK;
            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.ToString());
            }
        }

        private void checkBoxClanarina_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxClanarina.Checked)
            {
                label11.Visible = true;
                txtTipClanarine.Visible = true;
                label14.Visible = true;
                labDatumIsteka.Visible = true;
                labDatumPocetka.Visible = true;
                txtCenaClanarine.Visible = true;
                dtDatumPocetka.Visible = true;
                dtDatumIsteka.Visible = true;
                btnDodajTipClanarine.Visible = true;
                checkedListBoxTipClanarine.Visible = true;
                label12.Visible = true;
            }
            else
            {
                label11.Visible = false;
                label14.Visible = false;
                labDatumIsteka.Visible = false;
                labDatumPocetka.Visible = false;
                txtTipClanarine.Visible = false;
                dtDatumPocetka.Visible = false;
                dtDatumIsteka.Visible = false;
                txtCenaClanarine.Visible = false;
                btnDodajTipClanarine.Visible = false;
                checkedListBoxTipClanarine.Visible = false;
                label12.Visible = false;
            }
        }

        private void txtTipClanarine_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnDodajTipClanarine_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtTipClanarine.Text != "" && txtCenaClanarine.Text!= "" )
                {
                    Prihod.Clanarina t = new Prihod.Clanarina();
                    t.Tip_Clanarine =  txtTipClanarine.Text ;
                    t.Cena_Clanarine =  int.Parse(txtCenaClanarine.Text);
                    t.Datum_Pocetka = DateTime.ParseExact(dtDatumPocetka.Value.ToString("dd-MM-yyyy"), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    t.Datum_Isteka= DateTime.ParseExact(dtDatumIsteka.Value.ToString("dd-MM-yyyy"), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    DTOManager.KreirajTipClanarine(t);
                    this.PopuniListuTipovaClanarina();

                    txtCenaClanarine.Clear();
                    txtTipClanarine.Clear();

                    dtDatumPocetka.CustomFormat = "";
                    dtDatumIsteka.CustomFormat = "";
                    dtDatumPocetka.Format = DateTimePickerFormat.Custom;
                    dtDatumIsteka.Format =  DateTimePickerFormat.Custom; 


                }
            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.ToString());
            }
        }

        private void checkedListBoxTipClanarine_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void txtCenaClanarine_TextChanged(object sender, EventArgs e)
        {

        }

        private void label13_Click(object sender, EventArgs e)
        {

        }
    }
}
