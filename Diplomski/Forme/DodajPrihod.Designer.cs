﻿
namespace Diplomski.Forme
{
    partial class DodajPrihod
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnDodajClanarinu = new System.Windows.Forms.Button();
            this.btnDodajPublikaciju = new System.Windows.Forms.Button();
            this.btnDodajUslugu = new System.Windows.Forms.Button();
            this.btnDodajDonaciju = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnDodajClanarinu
            // 
            this.btnDodajClanarinu.Location = new System.Drawing.Point(196, 96);
            this.btnDodajClanarinu.Name = "btnDodajClanarinu";
            this.btnDodajClanarinu.Size = new System.Drawing.Size(154, 51);
            this.btnDodajClanarinu.TabIndex = 0;
            this.btnDodajClanarinu.Text = "Dodaj Clanarinu";
            this.btnDodajClanarinu.UseVisualStyleBackColor = true;
            this.btnDodajClanarinu.Click += new System.EventHandler(this.btnDodajClanarinu_Click);
            // 
            // btnDodajPublikaciju
            // 
            this.btnDodajPublikaciju.Location = new System.Drawing.Point(583, 96);
            this.btnDodajPublikaciju.Name = "btnDodajPublikaciju";
            this.btnDodajPublikaciju.Size = new System.Drawing.Size(148, 51);
            this.btnDodajPublikaciju.TabIndex = 1;
            this.btnDodajPublikaciju.Text = "Dodaj Publikaciju";
            this.btnDodajPublikaciju.UseVisualStyleBackColor = true;
            this.btnDodajPublikaciju.Click += new System.EventHandler(this.btnDodajPublikaciju_Click);
            // 
            // btnDodajUslugu
            // 
            this.btnDodajUslugu.Location = new System.Drawing.Point(196, 370);
            this.btnDodajUslugu.Name = "btnDodajUslugu";
            this.btnDodajUslugu.Size = new System.Drawing.Size(154, 54);
            this.btnDodajUslugu.TabIndex = 2;
            this.btnDodajUslugu.Text = "Dodaj Uslugu";
            this.btnDodajUslugu.UseVisualStyleBackColor = true;
            this.btnDodajUslugu.Click += new System.EventHandler(this.btnDodajUslugu_Click);
            // 
            // btnDodajDonaciju
            // 
            this.btnDodajDonaciju.Location = new System.Drawing.Point(583, 370);
            this.btnDodajDonaciju.Name = "btnDodajDonaciju";
            this.btnDodajDonaciju.Size = new System.Drawing.Size(148, 54);
            this.btnDodajDonaciju.TabIndex = 3;
            this.btnDodajDonaciju.Text = "Dodaj Donaciju";
            this.btnDodajDonaciju.UseVisualStyleBackColor = true;
            this.btnDodajDonaciju.Click += new System.EventHandler(this.btnDodajDonaciju_Click);
            // 
            // DodajPrihod
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(881, 513);
            this.Controls.Add(this.btnDodajDonaciju);
            this.Controls.Add(this.btnDodajUslugu);
            this.Controls.Add(this.btnDodajPublikaciju);
            this.Controls.Add(this.btnDodajClanarinu);
            this.Name = "DodajPrihod";
            this.Text = "DodajPrihod";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnDodajClanarinu;
        private System.Windows.Forms.Button btnDodajPublikaciju;
        private System.Windows.Forms.Button btnDodajUslugu;
        private System.Windows.Forms.Button btnDodajDonaciju;
    }
}