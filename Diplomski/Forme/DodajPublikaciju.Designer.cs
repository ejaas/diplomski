﻿
namespace Diplomski.Forme
{
    partial class DodajPublikaciju
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnSnimi = new System.Windows.Forms.Button();
            this.labelTipPublikacije = new System.Windows.Forms.Label();
            this.btnDodajPublikaciju = new System.Windows.Forms.Button();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.txtPublikacija = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // btnSnimi
            // 
            this.btnSnimi.Location = new System.Drawing.Point(573, 369);
            this.btnSnimi.Name = "btnSnimi";
            this.btnSnimi.Size = new System.Drawing.Size(190, 45);
            this.btnSnimi.TabIndex = 35;
            this.btnSnimi.Text = "Snimi";
            this.btnSnimi.UseVisualStyleBackColor = true;
            this.btnSnimi.Click += new System.EventHandler(this.btnSnimi_Click);
            // 
            // labelTipPublikacije
            // 
            this.labelTipPublikacije.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTipPublikacije.Location = new System.Drawing.Point(28, 65);
            this.labelTipPublikacije.Name = "labelTipPublikacije";
            this.labelTipPublikacije.Size = new System.Drawing.Size(150, 97);
            this.labelTipPublikacije.TabIndex = 33;
            this.labelTipPublikacije.Text = "Tip publikacije";
            this.labelTipPublikacije.Click += new System.EventHandler(this.labelTipPublikacije_Click);
            // 
            // btnDodajPublikaciju
            // 
            this.btnDodajPublikaciju.Location = new System.Drawing.Point(486, 55);
            this.btnDodajPublikaciju.Name = "btnDodajPublikaciju";
            this.btnDodajPublikaciju.Size = new System.Drawing.Size(120, 52);
            this.btnDodajPublikaciju.TabIndex = 38;
            this.btnDodajPublikaciju.Text = "Dodaj publikacije";
            this.btnDodajPublikaciju.UseVisualStyleBackColor = true;
            this.btnDodajPublikaciju.Click += new System.EventHandler(this.btnDodajPublikaciju_Click);
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.ItemHeight = 20;
            this.listBox1.Location = new System.Drawing.Point(219, 129);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(242, 124);
            this.listBox1.TabIndex = 37;
            this.listBox1.SelectedIndexChanged += new System.EventHandler(this.listBox1_SelectedIndexChanged);
            // 
            // txtPublikacija
            // 
            this.txtPublikacija.Location = new System.Drawing.Point(219, 67);
            this.txtPublikacija.Name = "txtPublikacija";
            this.txtPublikacija.Size = new System.Drawing.Size(242, 26);
            this.txtPublikacija.TabIndex = 36;
            this.txtPublikacija.TextChanged += new System.EventHandler(this.txtPublikacija_TextChanged);
            // 
            // DodajPublikaciju
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.btnDodajPublikaciju);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.txtPublikacija);
            this.Controls.Add(this.btnSnimi);
            this.Controls.Add(this.labelTipPublikacije);
            this.Name = "DodajPublikaciju";
            this.Text = "DodajPublikaciju";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btnSnimi;
        private System.Windows.Forms.Label labelTipPublikacije;
        private System.Windows.Forms.Button btnDodajPublikaciju;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.TextBox txtPublikacija;
    }
}