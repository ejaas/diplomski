﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Diplomski.Entiteti;
using DocumentFormat.OpenXml.Office.Drawing;
using NHibernate;
using static Diplomski.DTOs;
using System.Drawing;

using System.Drawing.Printing;

namespace Diplomski.Forme
{
    public partial class ModifikujClana : Form
    {
        private Clan cl;
        Bitmap bmp;
        Font printFont;
        Bitmap bitmap;
        public ModifikujClana()
        {
            InitializeComponent();
            cl = null;
            btnZene.Visible = false;
            btnMuskarci.Visible = false;
            btnSvi.Visible = false;
            btnAktivni.Visible = false;
            btnNeaktivni.Visible = false;
            printDocument1.PrintPage += new PrintPageEventHandler(printDocument1_PrintPage);

        }

        private void ModifikujClana_Load(object sender, EventArgs e)
        {
            try
            {
                this.PopuniListuClanova();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void PopuniListuClanova()
        {
            IList<ClanInfo> clanovi = DTOManager.VratiClanove();


            foreach (ClanInfo n in clanovi)
            {
                ListViewItem item = new ListViewItem(new string[] { n.IdClan.ToString(), n.Ime, n.Prezime, n.Email, n.Telefon.ToString(), n.Adresa, n.Rod, n.Jmbg.ToString(), n.brLk.ToString(), n.Imeroditelja });

                listView2.Items.Add(item);
               
            }
            listView2.Refresh();
        }

        private void listView2_SelectedIndexChanged(object sender, EventArgs e)
        {

            try
            {
                if (listView2.SelectedItems.Count > 0)
                {
                    ISession s = DataLayer.GetSession();

                    cl = s.Load<Clan>(int.Parse(listView2.SelectedItems[0].Text));
                    txtIme.Text = cl.Ime;
                    txtPrezime.Text = cl.Prezime;
                    txtEmail.Text = cl.Email;
                    txtTelefon.Text = cl.Telefon.ToString();
                    txtAdresa.Text = cl.Adresa;
                    txtJmbg.Text = cl.Jmbg.ToString();
                    txtBrLK.Text = cl.BrLicneKarte.ToString();
                    txtRoditelj.Text = cl.ImeRoditelja;

                  
                    
                    


                    s.Close();
                }
            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.ToString());
            }

        }


        private void button2_Click(object sender, EventArgs e)
        {

            try
            {
                ISession s = DataLayer.GetSession();

                cl.Ime = txtIme.Text;
                cl.Prezime = txtPrezime.Text;
                cl.Email = txtEmail.Text;
                cl.Telefon = int.Parse(txtTelefon.Text);
                cl.Adresa = txtAdresa.Text;
                cl.Jmbg = int.Parse(txtJmbg.Text);
                cl.BrLicneKarte = int.Parse(txtBrLK.Text);
                cl.ImeRoditelja = txtRoditelj.Text;

                s.SaveOrUpdate(cl);
                s.Flush();

                s.Close();

                this.DialogResult = DialogResult.OK;
            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.ToString());
            }

        }

        private void CheckSvi_CheckedChanged(object sender, EventArgs e)
        {


            if (CheckSvi.Checked)
            {
                btnSvi.Visible = true;

            }
            else
            {
                btnSvi.Visible = false;



            }
        }

        private void CheckMuskarci_CheckedChanged(object sender, EventArgs e)
        {

            if (CheckMuskarci.Checked)
            {
                btnMuskarci.Visible = true;

            }
            else
            {
                btnMuskarci.Visible = false;



            }
        }

        private void CheckZene_CheckedChanged(object sender, EventArgs e)
        {


            if (CheckZene.Checked)
            {
                btnZene.Visible = true;

            }
            else
            {
                btnZene.Visible = false;



            }
        }

        private void CheckAktivni_CheckedChanged(object sender, EventArgs e)
        {
            if (CheckAktivni.Checked)
            {
                btnAktivni.Visible = true;

            }
            else
            {
                btnAktivni.Visible = false;

            }
        }

        private void CheckNeaktivni_CheckedChanged(object sender, EventArgs e)
        {
            if (CheckNeaktivni.Checked)
            {
                btnNeaktivni.Visible = true;

            }
            else
            {
                btnNeaktivni.Visible = false;

            }
        }

        private void txtBrojClanova_TextChanged(object sender, EventArgs e)
        {
           
        }

        private void btnBroj_Click(object sender, EventArgs e)
        {
            txtBrojClanova.Clear();

            try
            {


                    txtBrojClanova.Text = listView2.Items.Count.ToString();


            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }
        }
         
        private void btnSvi_Click(object sender, EventArgs e)
        {
            listView2.Items.Clear();

            try
            {
                ISession s = DataLayer.GetSession();

                IQuery q = s.CreateQuery("from Clan");

                // IEnumerable<Clan> clanovi = q.Enumerable<Clan>();
                IList<ClanInfo> clanovi = DTOManager.VratiClanove();


                //  MessageBox.Show("Ime: " + o.Ime + " Prezime: " + o.Prezime);



                foreach (ClanInfo n in clanovi)
                {
                    if (n.Rod == "M" || n.Rod == "Z")
                    {
                        listView2.Refresh();
                        ListViewItem item = new ListViewItem(new string[] { n.IdClan.ToString(), n.Ime, n.Prezime, n.Email, n.Telefon.ToString(), n.Adresa, n.Rod, n.Jmbg.ToString(), n.brLk.ToString(), n.Imeroditelja });

                        listView2.Items.Add(item);
                        
                    }
                }
                listView2.Refresh();



                s.Close();

            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }
        }

        private void btnMuskarci_Click(object sender, EventArgs e)
        {
            listView2.Items.Clear();

            try
            {
                ISession s = DataLayer.GetSession();

                IQuery q = s.CreateQuery("from Clan");

               // IEnumerable<Clan> clanovi = q.Enumerable<Clan>();
                IList<ClanInfo> clanovi = DTOManager.VratiClanove();

                 
                      //  MessageBox.Show("Ime: " + o.Ime + " Prezime: " + o.Prezime);

                   

                    foreach (ClanInfo n in clanovi)
                    {
                    if (n.Rod == "M")
                    {
                        listView2.Refresh();
                        ListViewItem item = new ListViewItem(new string[] { n.IdClan.ToString(), n.Ime, n.Prezime, n.Email, n.Telefon.ToString(), n.Adresa, n.Rod, n.Jmbg.ToString(), n.brLk.ToString(), n.Imeroditelja });

                        listView2.Items.Add(item);
                    }
                    }
                    listView2.Refresh();

                

                s.Close();

            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }
        }

        private void btnZene_Click(object sender, EventArgs e)
        {
            listView2.Items.Clear();

            try
            {
                ISession s = DataLayer.GetSession();

                IQuery q = s.CreateQuery("from Clan");

                // IEnumerable<Clan> clanovi = q.Enumerable<Clan>();
                IList<ClanInfo> clanovi = DTOManager.VratiClanove();


                //  MessageBox.Show("Ime: " + o.Ime + " Prezime: " + o.Prezime);



                foreach (ClanInfo n in clanovi)
                {

                    if (n.Rod == "Z")
                    {
                       
                        ListViewItem item = new ListViewItem(new string[] { n.IdClan.ToString(), n.Ime, n.Prezime, n.Email, n.Telefon.ToString(), n.Adresa, n.Rod, n.Jmbg.ToString(), n.brLk.ToString(), n.Imeroditelja });

                        listView2.Items.Add(item);
                    }
                }
                listView2.Refresh();



                s.Close();

            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtIme_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnAktivni_Click(object sender, EventArgs e)
        {
            listView2.Items.Clear();

            try
            {
                ISession s = DataLayer.GetSession();

                IQuery q = s.CreateQuery("from Clan");

                // IEnumerable<Clan> clanovi = q.Enumerable<Clan>();
                IList<ClanInfo> clanovi = DTOManager.VratiClanove();


                //  MessageBox.Show("Ime: " + o.Ime + " Prezime: " + o.Prezime);



                foreach (ClanInfo n in clanovi)
                {

                    if (n.IdClan < 130)
                    {

                        ListViewItem item = new ListViewItem(new string[] { n.IdClan.ToString(), n.Ime, n.Prezime, n.Email, n.Telefon.ToString(), n.Adresa, n.Rod, n.Jmbg.ToString(), n.brLk.ToString(), n.Imeroditelja });

                        listView2.Items.Add(item);
                    }
                }
                listView2.Refresh();



                s.Close();

            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }
        }

        private void btnNeaktivni_Click(object sender, EventArgs e)
        {
            listView2.Items.Clear();

            try
            {
                ISession s = DataLayer.GetSession();

                IQuery q = s.CreateQuery("from Clan");

                // IEnumerable<Clan> clanovi = q.Enumerable<Clan>();
                IList<ClanInfo> clanovi = DTOManager.VratiClanove();


                //  MessageBox.Show("Ime: " + o.Ime + " Prezime: " + o.Prezime);



                foreach (ClanInfo n in clanovi)
                {

                    if (n.IdClan > 130)
                    {

                        ListViewItem item = new ListViewItem(new string[] { n.IdClan.ToString(), n.Ime, n.Prezime, n.Email, n.Telefon.ToString(), n.Adresa, n.Rod, n.Jmbg.ToString(), n.brLk.ToString(), n.Imeroditelja });

                        listView2.Items.Add(item);
                    }
                }
                listView2.Refresh();



                s.Close();

            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }
        }

       

        private void printDocument1_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            
            e.Graphics.DrawString(txtIme.Text, new Font("Times New Roman", 14, FontStyle.Regular), Brushes.Black, new PointF( 100, 100) );
           
        }

   
        private void btnStampajClan_Click(object sender, EventArgs e)
        {

           
            if (printDialog1.ShowDialog() == DialogResult.OK)
            {
                printDocument1.Print();
            }
        }

        
    }
}
