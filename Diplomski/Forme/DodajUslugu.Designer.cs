﻿
namespace Diplomski.Forme
{
    partial class DodajUslugu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnDodajPublikaciju = new System.Windows.Forms.Button();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.txtUsluga = new System.Windows.Forms.TextBox();
            this.btnSnimi = new System.Windows.Forms.Button();
            this.labelTipUsluge = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnDodajPublikaciju
            // 
            this.btnDodajPublikaciju.Location = new System.Drawing.Point(491, 46);
            this.btnDodajPublikaciju.Name = "btnDodajPublikaciju";
            this.btnDodajPublikaciju.Size = new System.Drawing.Size(120, 52);
            this.btnDodajPublikaciju.TabIndex = 43;
            this.btnDodajPublikaciju.Text = "Dodaj Usluge";
            this.btnDodajPublikaciju.UseVisualStyleBackColor = true;
            this.btnDodajPublikaciju.Click += new System.EventHandler(this.btnDodajPublikaciju_Click);
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.ItemHeight = 20;
            this.listBox1.Location = new System.Drawing.Point(224, 120);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(242, 124);
            this.listBox1.TabIndex = 42;
            // 
            // txtUsluga
            // 
            this.txtUsluga.Location = new System.Drawing.Point(224, 58);
            this.txtUsluga.Name = "txtUsluga";
            this.txtUsluga.Size = new System.Drawing.Size(242, 26);
            this.txtUsluga.TabIndex = 41;
            // 
            // btnSnimi
            // 
            this.btnSnimi.Location = new System.Drawing.Point(578, 360);
            this.btnSnimi.Name = "btnSnimi";
            this.btnSnimi.Size = new System.Drawing.Size(190, 45);
            this.btnSnimi.TabIndex = 40;
            this.btnSnimi.Text = "Snimi";
            this.btnSnimi.UseVisualStyleBackColor = true;
            this.btnSnimi.Click += new System.EventHandler(this.btnSnimi_Click);
            // 
            // labelTipUsluge
            // 
            this.labelTipUsluge.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTipUsluge.Location = new System.Drawing.Point(33, 56);
            this.labelTipUsluge.Name = "labelTipUsluge";
            this.labelTipUsluge.Size = new System.Drawing.Size(150, 97);
            this.labelTipUsluge.TabIndex = 39;
            this.labelTipUsluge.Text = "Tip usluge";
            // 
            // DodajUslugu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.btnDodajPublikaciju);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.txtUsluga);
            this.Controls.Add(this.btnSnimi);
            this.Controls.Add(this.labelTipUsluge);
            this.Name = "DodajUslugu";
            this.Text = "DodajUslugu";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnDodajPublikaciju;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.TextBox txtUsluga;
        private System.Windows.Forms.Button btnSnimi;
        private System.Windows.Forms.Label labelTipUsluge;
    }
}