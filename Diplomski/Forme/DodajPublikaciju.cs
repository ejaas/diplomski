﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Diplomski.Entiteti;
using NHibernate;

namespace Diplomski.Forme
{
    public partial class DodajPublikaciju : Form
    {
        public DodajPublikaciju()
        {
            InitializeComponent();
        }

        private void btnDodajPublikaciju_Click(object sender, EventArgs e)
        {
            listBox1.Items.Add(txtPublikacija.Text);
            txtPublikacija.Text = "";
        }

        private void btnSnimi_Click(object sender, EventArgs e)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                foreach (string i in listBox1.Items)
                {
                    Prihod.Publikacija p = new Prihod.Publikacija()
                    {

                        Tip_Publikacije = i
                    };

                    s.Save(p);
                    s.Flush();
                }
            

                

                s.Close();
                this.DialogResult = DialogResult.OK;
            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.ToString());
            }
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void txtPublikacija_TextChanged(object sender, EventArgs e)
        {

        }

        private void labelTipPublikacije_Click(object sender, EventArgs e)
        {

        }
    }
}
