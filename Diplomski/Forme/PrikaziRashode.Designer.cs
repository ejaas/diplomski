﻿
namespace Diplomski.Forme
{
    partial class PrikaziRashode
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listView2 = new System.Windows.Forms.ListView();
            this.columnIdRashoda = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnTipRashoda = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnCena = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnIzdavaoc = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnDatumIzdavanja = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.SuspendLayout();
            // 
            // listView2
            // 
            this.listView2.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnIdRashoda,
            this.columnTipRashoda,
            this.columnCena,
            this.columnIzdavaoc,
            this.columnDatumIzdavanja});
            this.listView2.HideSelection = false;
            this.listView2.Location = new System.Drawing.Point(137, 130);
            this.listView2.Name = "listView2";
            this.listView2.Size = new System.Drawing.Size(782, 285);
            this.listView2.TabIndex = 76;
            this.listView2.UseCompatibleStateImageBehavior = false;
            this.listView2.View = System.Windows.Forms.View.Details;
            this.listView2.SelectedIndexChanged += new System.EventHandler(this.listView2_SelectedIndexChanged);
            // 
            // columnIdRashoda
            // 
            this.columnIdRashoda.Text = "Id Rashoda";
            this.columnIdRashoda.Width = 134;
            // 
            // columnTipRashoda
            // 
            this.columnTipRashoda.Text = "Tip Rashoda";
            this.columnTipRashoda.Width = 131;
            // 
            // columnCena
            // 
            this.columnCena.Text = "Cena";
            this.columnCena.Width = 139;
            // 
            // columnIzdavaoc
            // 
            this.columnIzdavaoc.Text = "Izdavaoc";
            this.columnIzdavaoc.Width = 130;
            // 
            // columnDatumIzdavanja
            // 
            this.columnDatumIzdavanja.Text = "Datum Izdavanja";
            this.columnDatumIzdavanja.Width = 159;
            // 
            // PrikaziRashode
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1183, 648);
            this.Controls.Add(this.listView2);
            this.Name = "PrikaziRashode";
            this.Text = "PrikaziRashode";
            this.Load += new System.EventHandler(this.ModifikujRashod_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListView listView2;
        private System.Windows.Forms.ColumnHeader columnIdRashoda;
        private System.Windows.Forms.ColumnHeader columnTipRashoda;
        private System.Windows.Forms.ColumnHeader columnCena;
        private System.Windows.Forms.ColumnHeader columnIzdavaoc;
        private System.Windows.Forms.ColumnHeader columnDatumIzdavanja;
    }
}