﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Diplomski.Entiteti;
using NHibernate;
using static Diplomski.DTOs;


namespace Diplomski.Forme
{
   
    public partial class PrikaziRashode : Form
    {
        private Rashod rs;
        public PrikaziRashode()
        {
            InitializeComponent();
            rs = null;
        }

        private void listView2_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (listView2.SelectedItems.Count > 0)
                {
                    ISession s = DataLayer.GetSession();

                    rs = s.Load<Rashod>(int.Parse(listView2.SelectedItems[0].Text));

                    s.Close();
                }
            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.ToString());
            }
        }

        private void PopuniListuRashoda()
        {
            IList<RashodInfo> rashod = DTOManager.VratiRashode();

            foreach (RashodInfo n in rashod)
            {
                ListViewItem item = new ListViewItem(new string[] { n.IdRashoda.ToString(), n.Tip_Rashoda, n.Cena.ToString(), n.Izdavaoc, n.Datum_Izdavanja.ToString() });

                listView2.Items.Add(item);
            }
            listView2.Refresh();
        }


        private void ModifikujRashod_Load(object sender, EventArgs e)
        {
            try
            {
                this.PopuniListuRashoda();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
    }
}
