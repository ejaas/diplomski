﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Diplomski.Entiteti;
using NHibernate;
using System.Globalization;



namespace Diplomski.Forme
{
    public partial class DodajClanarinu : Form
    {
        

        public DodajClanarinu()
        {
            InitializeComponent();
        }

        private void btnSnimi_Click(object sender, EventArgs e)
        {
            try
            {
                ISession s = DataLayer.GetSession();


                Prihod.Clanarina cl = new Prihod.Clanarina()
                {
                    Vreme_Vazenja = int.Parse(txtVremeVazenja.Text),
                    Tip_Clanarine = txtTipClanarine.Text,
                    Cena_Clanarine = int.Parse(txtCenaClanarine.Text),
                    Datum_Pocetka = DateTime.ParseExact(dtDatumOd.Value.ToString("dd-MM-yyyy"), "dd-MM-yyyy", CultureInfo.InvariantCulture),
                    Datum_Isteka = DateTime.ParseExact(dtDatumDo.Value.ToString("dd-MM-yyyy"), "dd-MM-yyyy", CultureInfo.InvariantCulture),
                   
                };

                s.Save(cl);
                s.Flush();
                s.Close();

                this.DialogResult = DialogResult.OK;

            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.ToString());
            }
        }

        private void btnDodajTip_Click(object sender, EventArgs e)
        {
           
        }

        private void dtDatumDo_ValueChanged(object sender, EventArgs e)
        {

        }

        private void txtTipClanarine_TextChanged(object sender, EventArgs e)
        {

        }

        private void dtDatumOd_ValueChanged(object sender, EventArgs e)
        {

        }

        private void labelDatumOd_Click(object sender, EventArgs e)
        {

        }

        private void labelDatumDo_Click(object sender, EventArgs e)
        {

        }

        private void labelTipClanarine_Click(object sender, EventArgs e)
        {

        }

        private void txtCenaClanarine_TextChanged(object sender, EventArgs e)
        {

        }

        private void labelCenaClanarine_Click(object sender, EventArgs e)
        {

        }

        private void txtVremeVazenja_TextChanged(object sender, EventArgs e)
        {
           
        }

        private void labelVremeVazenja_Click(object sender, EventArgs e)
        {
           
        }

        private void btnVremeVazenje_Click(object sender, EventArgs e)
        {
           

            try
            {

            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }
        }

        public void VratiRazlikuDatuma()
        {
            //TimeSpan vrati = (DatumPocetka.Date - DatumIsteka.Date);
            //Console.WriteLine(DatumPocetka + "" + DatumIsteka + "" + "" + vrat + "");
        }

        private void DodajClanarinu_Load(object sender, EventArgs e)
        {

        }
    }
}
