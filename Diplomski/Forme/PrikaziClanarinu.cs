﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Diplomski.Entiteti;
using NHibernate;
using static Diplomski.DTOs;

namespace Diplomski.Forme
{
    public partial class PrikaziClanarinu : Form
    {
        private Prihod.Clanarina pr;
        public PrikaziClanarinu()
        {
            InitializeComponent();
            pr = null;
        }

        private void listView2_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (listView2.SelectedItems.Count > 0)
                {
                    ISession s = DataLayer.GetSession();

                    pr = s.Load<Prihod.Clanarina>(int.Parse(listView2.SelectedItems[0].Text));

                    dtDatumPocetka.Value = (pr.Datum_Pocetka == null ? DateTime.Now : (DateTime)pr.Datum_Pocetka);
                    dtDatumIsteka.Value = (pr.Datum_Isteka == null ? DateTime.Now : (DateTime)pr.Datum_Isteka);

                  //  TimeSpan vrati = pr.Datum_Pocetka - pr.Datum_Isteka;

                    s.Close();
                }
            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.ToString());
            }
        }

        private void PopuniListuPrihoda()
        {
            IList<ClanarinaInfo> clanarina = DTOManager.VratiClanarine();

            foreach (ClanarinaInfo n in clanarina)
            {
           ListViewItem item = new ListViewItem(new string[] { n.IdPrihoda.ToString(),  n.Tip_Clanarine, n.Cena_Clanarine.ToString(),  n.Datum_Pocetka.ToString(), n.Datum_Isteka.ToString()});

            listView2.Items.Add(item);
            }
            listView2.Refresh();
        }


        private void ModifikujClanarinu_Load(object sender, EventArgs e)
        {
            try
            {
                this.PopuniListuPrihoda();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            txtVremeVazenja.Clear();

            try
            {

              // TimeSpan vrati = (pr.Datum_Pocetka.Date - pr.Datum_Isteka.Date);

                TimeSpan vratibrojdana = pr.Datum_Isteka.Date.Subtract(pr.Datum_Pocetka);

                txtVremeVazenja.Text = vratibrojdana.ToString();

                //Console.WriteLine(DatumPocetka + "" + DatumIsteka + "" + "" + vrat + "");

            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }
        }

        private void txtVremeVazenja_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnPrikaziBrojPreostalihDana_Click(object sender, EventArgs e)
        {
            DateTime now = DateTime.Now;

            //TimeSpan vratipreostale = (pr.Datum_Pocetka.Date - now.Date);

            TimeSpan vratipreostale = now.Subtract(pr.Datum_Isteka);

            txtBrojPreostalihDana.Text = vratipreostale.ToString();


        }
    }
}
