﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using org.apache.pdfbox.pdmodel;
using org.apache.pdfbox.util;
using Diplomski.Entiteti;
using NHibernate;
using System.Globalization;

namespace Diplomski.Forme
{
    public partial class PDF : Form
    {
        public PDF()
        {
            InitializeComponent();
        }

        private void PDF_Load(object sender, EventArgs e)
        {

        }

        private void btnOpenPDF_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "PDF files(*.pdf)|*.pdf";
            if(ofd.ShowDialog() == DialogResult.OK)
            {
                string path = ofd.FileName.ToString();
                textBox1.Text = path;
            }    
        }

        private void btnKonvert_Click(object sender, EventArgs e)
        {
            try
            {
                
                PDDocument doc = PDDocument.load(textBox1.Text);
                PDFTextStripper stripper = new PDFTextStripper();
                stripper.setStartPage(1);
                stripper.setEndPage(5);
                richTextBox1.Text = (stripper.getText(doc));
                doc.close();

               
                }
            catch (Exception ec)
            {
                MessageBox.Show(ec.ToString());
            }

        }

        private void btnSacuvaj_Click(object sender, EventArgs e)
        {
            saveFileDialog1.DefaultExt = "*.rtf";
            saveFileDialog1.Filter = "RFT File|*.rtf";
            if(saveFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK && saveFileDialog1.FileName.Length > 0  )
            {
                richTextBox1.SaveFile(saveFileDialog1.FileName, RichTextBoxStreamType.PlainText);
            }
         }

        private void btnPretrazi_Click(object sender, EventArgs e)
        {
       

            string[] words= textBox2.Text.Split(':');
            foreach(string word in words)
            {
                int startIndex = 0;
                while(startIndex < richTextBox1.TextLength)
                {
                    int wordStartIndex = richTextBox1.Find(word, startIndex, RichTextBoxFinds.None);
                    if (wordStartIndex != -1)
                    {
                        richTextBox1.SelectionStart = wordStartIndex;
                        richTextBox1.SelectionLength = word.Length;
                        richTextBox1.SelectionColor = Color.Red;
                    }
                    else
                        break;
                    startIndex += wordStartIndex + word.Length;
                }
            }
        }

        private void btnPrikazi_Click(object sender, EventArgs e)
        {
            if (txtIme.Text == string.Empty)
            {
                txtIme.Text = richTextBox1.SelectedText;
                txtUplata.Text = "";
                txtBrRacuna.Text = "";

            }
            else if (txtUplata.Text == string.Empty)
            {
                txtUplata.Text = richTextBox1.SelectedText;
                txtBrRacuna.Text = "";
            }            

            else if (txtBrRacuna.Text == string.Empty)

            { txtBrRacuna.Text = richTextBox1.SelectedText; }

            else 
            {
                (dtDatum.Value) = Convert.ToDateTime(richTextBox1.SelectedText);
            }
        }

        private void btnPotvrda_Click(object sender, EventArgs e)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                Uplata cl = new Uplata()
                {
                    Ime_Clan = txtIme.Text,
                    Cena = float.Parse(txtUplata.Text),
                    Br_Racuna= int.Parse(txtBrRacuna.Text),
                    Datum = DateTime.ParseExact(dtDatum.Value.ToString("dd-MM-yyyy"), "dd-MM-yyyy", CultureInfo.InvariantCulture),


                };

                s.Save(cl);
                s.Flush();
                s.Close();

                this.DialogResult = DialogResult.OK;

            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.ToString());
            }
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }
    }
}
