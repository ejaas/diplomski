﻿
namespace Diplomski.Forme
{
    partial class PDF
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.btnOpenPDF = new System.Windows.Forms.Button();
            this.btnKonvert = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.btnSacuvaj = new System.Windows.Forms.Button();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.btnPretrazi = new System.Windows.Forms.Button();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.btnPrikazi = new System.Windows.Forms.Button();
            this.txtIme = new System.Windows.Forms.TextBox();
            this.txtUplata = new System.Windows.Forms.TextBox();
            this.lblIme = new System.Windows.Forms.Label();
            this.lblUplata = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnPotvrda = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtBrRacuna = new System.Windows.Forms.TextBox();
            this.dtDatum = new System.Windows.Forms.DateTimePicker();
            this.SuspendLayout();
            // 
            // richTextBox1
            // 
            this.richTextBox1.Location = new System.Drawing.Point(33, 12);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(489, 277);
            this.richTextBox1.TabIndex = 0;
            this.richTextBox1.Text = "";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(33, 335);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(325, 26);
            this.textBox1.TabIndex = 1;
            // 
            // btnOpenPDF
            // 
            this.btnOpenPDF.Location = new System.Drawing.Point(575, 109);
            this.btnOpenPDF.Name = "btnOpenPDF";
            this.btnOpenPDF.Size = new System.Drawing.Size(159, 55);
            this.btnOpenPDF.TabIndex = 2;
            this.btnOpenPDF.Text = "Učitaj PDF izvoda";
            this.btnOpenPDF.UseVisualStyleBackColor = true;
            this.btnOpenPDF.Click += new System.EventHandler(this.btnOpenPDF_Click);
            // 
            // btnKonvert
            // 
            this.btnKonvert.Location = new System.Drawing.Point(575, 190);
            this.btnKonvert.Name = "btnKonvert";
            this.btnKonvert.Size = new System.Drawing.Size(159, 55);
            this.btnKonvert.TabIndex = 3;
            this.btnKonvert.Text = "Prikazi pdf";
            this.btnKonvert.UseVisualStyleBackColor = true;
            this.btnKonvert.Click += new System.EventHandler(this.btnKonvert_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // btnSacuvaj
            // 
            this.btnSacuvaj.Location = new System.Drawing.Point(575, 273);
            this.btnSacuvaj.Name = "btnSacuvaj";
            this.btnSacuvaj.Size = new System.Drawing.Size(159, 55);
            this.btnSacuvaj.TabIndex = 4;
            this.btnSacuvaj.Text = "Sacuvaj";
            this.btnSacuvaj.UseVisualStyleBackColor = true;
            this.btnSacuvaj.Click += new System.EventHandler(this.btnSacuvaj_Click);
            // 
            // btnPretrazi
            // 
            this.btnPretrazi.Location = new System.Drawing.Point(394, 447);
            this.btnPretrazi.Name = "btnPretrazi";
            this.btnPretrazi.Size = new System.Drawing.Size(159, 50);
            this.btnPretrazi.TabIndex = 5;
            this.btnPretrazi.Text = "Pretrazi ";
            this.btnPretrazi.UseVisualStyleBackColor = true;
            this.btnPretrazi.Click += new System.EventHandler(this.btnPretrazi_Click);
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(33, 457);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(325, 26);
            this.textBox2.TabIndex = 6;
            // 
            // btnPrikazi
            // 
            this.btnPrikazi.Location = new System.Drawing.Point(848, 446);
            this.btnPrikazi.Name = "btnPrikazi";
            this.btnPrikazi.Size = new System.Drawing.Size(159, 34);
            this.btnPrikazi.TabIndex = 7;
            this.btnPrikazi.Text = "Potvrdi ";
            this.btnPrikazi.UseVisualStyleBackColor = true;
            this.btnPrikazi.Click += new System.EventHandler(this.btnPrikazi_Click);
            // 
            // txtIme
            // 
            this.txtIme.Location = new System.Drawing.Point(917, 46);
            this.txtIme.Name = "txtIme";
            this.txtIme.Size = new System.Drawing.Size(181, 26);
            this.txtIme.TabIndex = 8;
            // 
            // txtUplata
            // 
            this.txtUplata.Location = new System.Drawing.Point(917, 94);
            this.txtUplata.Name = "txtUplata";
            this.txtUplata.Size = new System.Drawing.Size(181, 26);
            this.txtUplata.TabIndex = 9;
            // 
            // lblIme
            // 
            this.lblIme.AutoSize = true;
            this.lblIme.Location = new System.Drawing.Point(846, 52);
            this.lblIme.Name = "lblIme";
            this.lblIme.Size = new System.Drawing.Size(44, 20);
            this.lblIme.TabIndex = 10;
            this.lblIme.Text = "Ime: ";
            // 
            // lblUplata
            // 
            this.lblUplata.AutoSize = true;
            this.lblUplata.Location = new System.Drawing.Point(830, 97);
            this.lblUplata.Name = "lblUplata";
            this.lblUplata.Size = new System.Drawing.Size(60, 20);
            this.lblUplata.TabIndex = 11;
            this.lblUplata.Text = "Uplata:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(29, 423);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(351, 20);
            this.label1.TabIndex = 12;
            this.label1.Text = "Unesi pretragu za ime (ime:) ili za uplatu (uplata:)";
            // 
            // btnPotvrda
            // 
            this.btnPotvrda.Location = new System.Drawing.Point(863, 234);
            this.btnPotvrda.Name = "btnPotvrda";
            this.btnPotvrda.Size = new System.Drawing.Size(243, 34);
            this.btnPotvrda.TabIndex = 13;
            this.btnPotvrda.Text = "Potvrdi podatke iz učitanog izvoda";
            this.btnPotvrda.UseVisualStyleBackColor = true;
            this.btnPotvrda.Click += new System.EventHandler(this.btnPotvrda_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(781, 423);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(312, 20);
            this.label2.TabIndex = 14;
            this.label2.Text = "Selektuj ime korisnika i uplatu koju je izvršio";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(29, 312);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(468, 20);
            this.label3.TabIndex = 15;
            this.label3.Text = "Možete uneti direktno putanju ili pretražiti na dugme za učitavanje";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(830, 185);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 20);
            this.label4.TabIndex = 16;
            this.label4.Text = "Datum: ";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(822, 144);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(89, 20);
            this.label5.TabIndex = 17;
            this.label5.Text = "Br.Racuna:";
            // 
            // txtBrRacuna
            // 
            this.txtBrRacuna.Location = new System.Drawing.Point(917, 138);
            this.txtBrRacuna.Name = "txtBrRacuna";
            this.txtBrRacuna.Size = new System.Drawing.Size(181, 26);
            this.txtBrRacuna.TabIndex = 19;
            // 
            // dtDatum
            // 
            this.dtDatum.Location = new System.Drawing.Point(917, 180);
            this.dtDatum.Name = "dtDatum";
            this.dtDatum.Size = new System.Drawing.Size(181, 26);
            this.dtDatum.TabIndex = 20;
            // 
            // PDF
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1298, 590);
            this.Controls.Add(this.dtDatum);
            this.Controls.Add(this.txtBrRacuna);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnPotvrda);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblUplata);
            this.Controls.Add(this.lblIme);
            this.Controls.Add(this.txtUplata);
            this.Controls.Add(this.txtIme);
            this.Controls.Add(this.btnPrikazi);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.btnPretrazi);
            this.Controls.Add(this.btnSacuvaj);
            this.Controls.Add(this.btnKonvert);
            this.Controls.Add(this.btnOpenPDF);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.richTextBox1);
            this.Name = "PDF";
            this.Text = "PDF";
            this.Load += new System.EventHandler(this.PDF_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button btnOpenPDF;
        private System.Windows.Forms.Button btnKonvert;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button btnSacuvaj;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.Button btnPretrazi;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Button btnPrikazi;
        private System.Windows.Forms.TextBox txtIme;
        private System.Windows.Forms.TextBox txtUplata;
        private System.Windows.Forms.Label lblIme;
        private System.Windows.Forms.Label lblUplata;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnPotvrda;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtBrRacuna;
        private System.Windows.Forms.DateTimePicker dtDatum;
    }
}