﻿
namespace Diplomski.Forme
{
    partial class ModifikujClana
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listView2 = new System.Windows.Forms.ListView();
            this.columnIDCLAN = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnIme = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnPrezime = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnEmail = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnTelefon = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnAdresa = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnRod = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnJmbg = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnBrLk = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnImeroditelja = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.txtAdresa = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtPrezime = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtIme = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.txtTelefon = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.CheckSvi = new System.Windows.Forms.CheckBox();
            this.CheckZene = new System.Windows.Forms.CheckBox();
            this.CheckMuskarci = new System.Windows.Forms.CheckBox();
            this.btnSvi = new System.Windows.Forms.Button();
            this.btnMuskarci = new System.Windows.Forms.Button();
            this.btnZene = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.CheckAktivni = new System.Windows.Forms.CheckBox();
            this.CheckNeaktivni = new System.Windows.Forms.CheckBox();
            this.btnAktivni = new System.Windows.Forms.Button();
            this.btnNeaktivni = new System.Windows.Forms.Button();
            this.txtBrojClanova = new System.Windows.Forms.TextBox();
            this.btnBroj = new System.Windows.Forms.Button();
            this.txtRoditelj = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtBrLK = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtJmbg = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.printDialog1 = new System.Windows.Forms.PrintDialog();
            this.printDocument1 = new System.Drawing.Printing.PrintDocument();
            this.btnStampajClan = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // listView2
            // 
            this.listView2.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnIDCLAN,
            this.columnIme,
            this.columnPrezime,
            this.columnEmail,
            this.columnTelefon,
            this.columnAdresa,
            this.columnRod,
            this.columnJmbg,
            this.columnBrLk,
            this.columnImeroditelja});
            this.listView2.HideSelection = false;
            this.listView2.Location = new System.Drawing.Point(2, 29);
            this.listView2.Name = "listView2";
            this.listView2.Size = new System.Drawing.Size(1150, 265);
            this.listView2.TabIndex = 74;
            this.listView2.UseCompatibleStateImageBehavior = false;
            this.listView2.View = System.Windows.Forms.View.Details;
            this.listView2.SelectedIndexChanged += new System.EventHandler(this.listView2_SelectedIndexChanged);
            // 
            // columnIDCLAN
            // 
            this.columnIDCLAN.Text = "Id";
            // 
            // columnIme
            // 
            this.columnIme.Text = "Ime";
            this.columnIme.Width = 95;
            // 
            // columnPrezime
            // 
            this.columnPrezime.Text = "Prezime";
            this.columnPrezime.Width = 117;
            // 
            // columnEmail
            // 
            this.columnEmail.Text = "Email";
            this.columnEmail.Width = 141;
            // 
            // columnTelefon
            // 
            this.columnTelefon.Text = "Telefon";
            this.columnTelefon.Width = 136;
            // 
            // columnAdresa
            // 
            this.columnAdresa.Text = "Adresa";
            this.columnAdresa.Width = 119;
            // 
            // columnRod
            // 
            this.columnRod.Text = "Rod";
            this.columnRod.Width = 81;
            // 
            // columnJmbg
            // 
            this.columnJmbg.Text = "Jmbg";
            this.columnJmbg.Width = 89;
            // 
            // columnBrLk
            // 
            this.columnBrLk.Text = "BrLK";
            this.columnBrLk.Width = 86;
            // 
            // columnImeroditelja
            // 
            this.columnImeroditelja.Text = "Roditelj";
            // 
            // txtAdresa
            // 
            this.txtAdresa.Location = new System.Drawing.Point(108, 529);
            this.txtAdresa.Name = "txtAdresa";
            this.txtAdresa.Size = new System.Drawing.Size(226, 26);
            this.txtAdresa.TabIndex = 70;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(36, 532);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(60, 20);
            this.label6.TabIndex = 69;
            this.label6.Text = "Adresa";
            // 
            // txtPrezime
            // 
            this.txtPrezime.Location = new System.Drawing.Point(108, 414);
            this.txtPrezime.Name = "txtPrezime";
            this.txtPrezime.Size = new System.Drawing.Size(226, 26);
            this.txtPrezime.TabIndex = 66;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(21, 417);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(66, 20);
            this.label4.TabIndex = 65;
            this.label4.Text = "Prezime";
            // 
            // txtIme
            // 
            this.txtIme.Location = new System.Drawing.Point(108, 348);
            this.txtIme.Name = "txtIme";
            this.txtIme.Size = new System.Drawing.Size(226, 26);
            this.txtIme.TabIndex = 64;
            this.txtIme.TextChanged += new System.EventHandler(this.txtIme_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(21, 351);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(36, 20);
            this.label3.TabIndex = 63;
            this.label3.Text = "Ime";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(270, 688);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(178, 33);
            this.button2.TabIndex = 77;
            this.button2.Text = "Snimi";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // txtTelefon
            // 
            this.txtTelefon.Location = new System.Drawing.Point(108, 581);
            this.txtTelefon.Name = "txtTelefon";
            this.txtTelefon.Size = new System.Drawing.Size(226, 26);
            this.txtTelefon.TabIndex = 81;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(36, 584);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(62, 20);
            this.label7.TabIndex = 80;
            this.label7.Text = "Telefon";
            // 
            // txtEmail
            // 
            this.txtEmail.Location = new System.Drawing.Point(108, 471);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(226, 26);
            this.txtEmail.TabIndex = 93;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(36, 474);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(48, 20);
            this.label5.TabIndex = 92;
            this.label5.Text = "Email";
            // 
            // CheckSvi
            // 
            this.CheckSvi.AutoSize = true;
            this.CheckSvi.Location = new System.Drawing.Point(731, 448);
            this.CheckSvi.Name = "CheckSvi";
            this.CheckSvi.Size = new System.Drawing.Size(56, 24);
            this.CheckSvi.TabIndex = 97;
            this.CheckSvi.Text = "Svi";
            this.CheckSvi.UseVisualStyleBackColor = true;
            this.CheckSvi.CheckedChanged += new System.EventHandler(this.CheckSvi_CheckedChanged);
            // 
            // CheckZene
            // 
            this.CheckZene.AutoSize = true;
            this.CheckZene.Location = new System.Drawing.Point(731, 512);
            this.CheckZene.Name = "CheckZene";
            this.CheckZene.Size = new System.Drawing.Size(72, 24);
            this.CheckZene.TabIndex = 98;
            this.CheckZene.Text = "Zene";
            this.CheckZene.UseVisualStyleBackColor = true;
            this.CheckZene.CheckedChanged += new System.EventHandler(this.CheckZene_CheckedChanged);
            // 
            // CheckMuskarci
            // 
            this.CheckMuskarci.AutoSize = true;
            this.CheckMuskarci.Location = new System.Drawing.Point(731, 478);
            this.CheckMuskarci.Name = "CheckMuskarci";
            this.CheckMuskarci.Size = new System.Drawing.Size(98, 24);
            this.CheckMuskarci.TabIndex = 99;
            this.CheckMuskarci.Text = "Muskarci";
            this.CheckMuskarci.UseVisualStyleBackColor = true;
            this.CheckMuskarci.CheckedChanged += new System.EventHandler(this.CheckMuskarci_CheckedChanged);
            // 
            // btnSvi
            // 
            this.btnSvi.Location = new System.Drawing.Point(971, 433);
            this.btnSvi.Name = "btnSvi";
            this.btnSvi.Size = new System.Drawing.Size(201, 30);
            this.btnSvi.TabIndex = 100;
            this.btnSvi.Text = "Pretrazi sve clanove";
            this.btnSvi.UseVisualStyleBackColor = true;
            this.btnSvi.Click += new System.EventHandler(this.btnSvi_Click);
            // 
            // btnMuskarci
            // 
            this.btnMuskarci.Location = new System.Drawing.Point(971, 469);
            this.btnMuskarci.Name = "btnMuskarci";
            this.btnMuskarci.Size = new System.Drawing.Size(201, 30);
            this.btnMuskarci.TabIndex = 101;
            this.btnMuskarci.Text = "Pretrazi muske clanove";
            this.btnMuskarci.UseVisualStyleBackColor = true;
            this.btnMuskarci.Click += new System.EventHandler(this.btnMuskarci_Click);
            // 
            // btnZene
            // 
            this.btnZene.Location = new System.Drawing.Point(971, 505);
            this.btnZene.Name = "btnZene";
            this.btnZene.Size = new System.Drawing.Size(201, 30);
            this.btnZene.TabIndex = 102;
            this.btnZene.Text = "Pretrazi zenske clanove";
            this.btnZene.UseVisualStyleBackColor = true;
            this.btnZene.Click += new System.EventHandler(this.btnZene_Click);
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(726, 395);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(287, 35);
            this.label1.TabIndex = 103;
            this.label1.Text = "Pretrazi clanove kluba";
            // 
            // CheckAktivni
            // 
            this.CheckAktivni.AutoSize = true;
            this.CheckAktivni.Location = new System.Drawing.Point(731, 612);
            this.CheckAktivni.Name = "CheckAktivni";
            this.CheckAktivni.Size = new System.Drawing.Size(156, 24);
            this.CheckAktivni.TabIndex = 104;
            this.CheckAktivni.Text = "Svi aktivni clanovi";
            this.CheckAktivni.UseVisualStyleBackColor = true;
            this.CheckAktivni.CheckedChanged += new System.EventHandler(this.CheckAktivni_CheckedChanged);
            // 
            // CheckNeaktivni
            // 
            this.CheckNeaktivni.AutoSize = true;
            this.CheckNeaktivni.Location = new System.Drawing.Point(731, 642);
            this.CheckNeaktivni.Name = "CheckNeaktivni";
            this.CheckNeaktivni.Size = new System.Drawing.Size(174, 24);
            this.CheckNeaktivni.TabIndex = 105;
            this.CheckNeaktivni.Text = "Svi neaktivni clanovi";
            this.CheckNeaktivni.UseVisualStyleBackColor = true;
            this.CheckNeaktivni.CheckedChanged += new System.EventHandler(this.CheckNeaktivni_CheckedChanged);
            // 
            // btnAktivni
            // 
            this.btnAktivni.Location = new System.Drawing.Point(971, 600);
            this.btnAktivni.Name = "btnAktivni";
            this.btnAktivni.Size = new System.Drawing.Size(201, 30);
            this.btnAktivni.TabIndex = 106;
            this.btnAktivni.Text = "Pretrazi sve aktivne";
            this.btnAktivni.UseVisualStyleBackColor = true;
            this.btnAktivni.Click += new System.EventHandler(this.btnAktivni_Click);
            // 
            // btnNeaktivni
            // 
            this.btnNeaktivni.Location = new System.Drawing.Point(971, 636);
            this.btnNeaktivni.Name = "btnNeaktivni";
            this.btnNeaktivni.Size = new System.Drawing.Size(201, 30);
            this.btnNeaktivni.TabIndex = 107;
            this.btnNeaktivni.Text = "Pretrazi sve neaktivne";
            this.btnNeaktivni.UseVisualStyleBackColor = true;
            this.btnNeaktivni.Click += new System.EventHandler(this.btnNeaktivni_Click);
            // 
            // txtBrojClanova
            // 
            this.txtBrojClanova.Location = new System.Drawing.Point(1112, 303);
            this.txtBrojClanova.Name = "txtBrojClanova";
            this.txtBrojClanova.Size = new System.Drawing.Size(40, 26);
            this.txtBrojClanova.TabIndex = 109;
            this.txtBrojClanova.TextChanged += new System.EventHandler(this.txtBrojClanova_TextChanged);
            // 
            // btnBroj
            // 
            this.btnBroj.Location = new System.Drawing.Point(971, 303);
            this.btnBroj.Name = "btnBroj";
            this.btnBroj.Size = new System.Drawing.Size(115, 30);
            this.btnBroj.TabIndex = 110;
            this.btnBroj.Text = "Broj Clanova";
            this.btnBroj.UseVisualStyleBackColor = true;
            this.btnBroj.Click += new System.EventHandler(this.btnBroj_Click);
            // 
            // txtRoditelj
            // 
            this.txtRoditelj.Location = new System.Drawing.Point(455, 457);
            this.txtRoditelj.Name = "txtRoditelj";
            this.txtRoditelj.Size = new System.Drawing.Size(226, 26);
            this.txtRoditelj.TabIndex = 116;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(374, 463);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(62, 20);
            this.label2.TabIndex = 115;
            this.label2.Text = "Roditelj";
            // 
            // txtBrLK
            // 
            this.txtBrLK.Location = new System.Drawing.Point(455, 404);
            this.txtBrLK.Name = "txtBrLK";
            this.txtBrLK.Size = new System.Drawing.Size(226, 26);
            this.txtBrLK.TabIndex = 114;
            this.txtBrLK.TextChanged += new System.EventHandler(this.textBox2_TextChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(374, 407);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(44, 20);
            this.label8.TabIndex = 113;
            this.label8.Text = "BrLK";
            // 
            // txtJmbg
            // 
            this.txtJmbg.Location = new System.Drawing.Point(455, 351);
            this.txtJmbg.Name = "txtJmbg";
            this.txtJmbg.Size = new System.Drawing.Size(226, 26);
            this.txtJmbg.TabIndex = 112;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(385, 351);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(48, 20);
            this.label9.TabIndex = 111;
            this.label9.Text = "Jmbg";
            // 
            // printDialog1
            // 
            this.printDialog1.UseEXDialog = true;
            // 
            // printDocument1
            // 
            this.printDocument1.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.printDocument1_PrintPage);
            // 
            // btnStampajClan
            // 
            this.btnStampajClan.Location = new System.Drawing.Point(1200, 76);
            this.btnStampajClan.Name = "btnStampajClan";
            this.btnStampajClan.Size = new System.Drawing.Size(143, 54);
            this.btnStampajClan.TabIndex = 119;
            this.btnStampajClan.Text = "Stampaj clanove";
            this.btnStampajClan.UseVisualStyleBackColor = true;
            this.btnStampajClan.Click += new System.EventHandler(this.btnStampajClan_Click);
            // 
            // ModifikujClana
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1777, 753);
            this.Controls.Add(this.btnStampajClan);
            this.Controls.Add(this.txtRoditelj);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtBrLK);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.txtJmbg);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.btnBroj);
            this.Controls.Add(this.txtBrojClanova);
            this.Controls.Add(this.btnNeaktivni);
            this.Controls.Add(this.btnAktivni);
            this.Controls.Add(this.CheckNeaktivni);
            this.Controls.Add(this.CheckAktivni);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnZene);
            this.Controls.Add(this.btnMuskarci);
            this.Controls.Add(this.btnSvi);
            this.Controls.Add(this.CheckMuskarci);
            this.Controls.Add(this.CheckZene);
            this.Controls.Add(this.CheckSvi);
            this.Controls.Add(this.txtEmail);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtTelefon);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.listView2);
            this.Controls.Add(this.txtAdresa);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtPrezime);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtIme);
            this.Controls.Add(this.label3);
            this.Name = "ModifikujClana";
            this.Text = "ModifikujClana";
            this.Load += new System.EventHandler(this.ModifikujClana_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ListView listView2;
        private System.Windows.Forms.ColumnHeader columnIme;
        private System.Windows.Forms.ColumnHeader columnPrezime;
        private System.Windows.Forms.TextBox txtAdresa;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtPrezime;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtIme;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox txtTelefon;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ColumnHeader columnEmail;
        private System.Windows.Forms.ColumnHeader columnTelefon;
        private System.Windows.Forms.ColumnHeader columnAdresa;
        private System.Windows.Forms.ColumnHeader columnIDCLAN;
        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ColumnHeader columnRod;
        private System.Windows.Forms.ColumnHeader columnJmbg;
        private System.Windows.Forms.ColumnHeader columnBrLk;
        private System.Windows.Forms.ColumnHeader columnImeroditelja;
        private System.Windows.Forms.CheckBox CheckSvi;
        private System.Windows.Forms.CheckBox CheckZene;
        private System.Windows.Forms.CheckBox CheckMuskarci;
        private System.Windows.Forms.Button btnSvi;
        private System.Windows.Forms.Button btnMuskarci;
        private System.Windows.Forms.Button btnZene;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox CheckAktivni;
        private System.Windows.Forms.CheckBox CheckNeaktivni;
        private System.Windows.Forms.Button btnAktivni;
        private System.Windows.Forms.Button btnNeaktivni;
        private System.Windows.Forms.TextBox txtBrojClanova;
        private System.Windows.Forms.Button btnBroj;
        private System.Windows.Forms.TextBox txtRoditelj;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtBrLK;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtJmbg;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.PrintDialog printDialog1;
        private System.Drawing.Printing.PrintDocument printDocument1;
        private System.Windows.Forms.Button btnStampajClan;
    }
}