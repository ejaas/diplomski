﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Diplomski.Entiteti
{
    public class Rashod
    {
        public virtual int IdRashoda { get; protected set; }

        public virtual string Tip_Rashoda { get; set; }

        public virtual int Cena { get; set; }

        public virtual string Izdavaoc { get; set; }

        public virtual DateTime DatumIzdavanja { get; set; }


       public Rashod()
        {

        }
    }
}
