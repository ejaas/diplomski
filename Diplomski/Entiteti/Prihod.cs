﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Diplomski.Entiteti
{
    public class Prihod
    {
        public virtual int IdPrihoda { get; protected set; }

        public virtual string Tip_Prihoda { get; set; }

        public virtual IList<Clan> Clanovi { get; set; }

        public virtual IList<Ima> ImaClanove { get; set; }

        public Prihod()
        {
            Clanovi = new List<Clan>();
            ImaClanove = new List<Ima>();
        }

        public class Clanarina : Prihod
        {
            public virtual string Tip_Clanarine { get; set; }

            public virtual int Cena_Clanarine { get; set; }

            public virtual int Vreme_Vazenja { get; set; }

            public virtual DateTime Datum_Pocetka { get; set; }

            public virtual DateTime Datum_Isteka { get; set; }
           public  Clanarina()
            {

            }
        }

        public class Usluga : Prihod
        {
            public virtual string Tip_Usluge { get; set; }
            public Usluga()
            {

            }
        }


        public class Publikacija : Prihod
        {
            public virtual string Tip_Publikacije { get; set; }
            public Publikacija()
            {

            }
        }


        public class Donacija : Prihod
        {
            public virtual string Tip_Donacije { get; set; }
            public Donacija()
            {

            }

        }


    }
}
