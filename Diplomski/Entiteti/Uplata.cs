﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Diplomski.Entiteti
{
    public class Uplata
    {
        public virtual string Ime_Clan { get; set; }

        public virtual float Cena { get; set; }

        public virtual int Id_Uplate { get; protected set; }

        public virtual int Br_Racuna { get; set; }

        public virtual DateTime Datum { get; set; }


        public Uplata()
        {

        }
    }
}
