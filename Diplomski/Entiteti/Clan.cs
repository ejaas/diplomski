﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Diplomski.Entiteti
{
    public class Clan
    {
        public virtual int IdClana { get; protected set; }

        public virtual string Ime { get; set; }

        public virtual string Prezime { get; set; }

        public virtual string Rod { get; set; }

        public virtual string ImeRoditelja { get; set; }

        public virtual string Email { get; set; }

        public virtual int Telefon { get; set; }

        public virtual string Adresa { get; set; }

        public virtual int Jmbg { get; set; }

        public virtual int BrLicneKarte { get; set; }


          public virtual IList<Prihod> Prihodi { get; set; }

        public virtual IList<Ima> ImaPrihode { get; set; }

        public Clan()
        {

                  Prihodi = new List<Prihod>();
                   ImaPrihode = new List<Ima>();
           

        }
    }


}
