﻿
namespace Diplomski
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.btnDodajClana = new System.Windows.Forms.Button();
            this.btnObrisiClana = new System.Windows.Forms.Button();
            this.btnIzmeniClana = new System.Windows.Forms.Button();
            this.btnDodajPrihod = new System.Windows.Forms.Button();
            this.btnDodajRashod = new System.Windows.Forms.Button();
            this.btnPrikaziClanarine = new System.Windows.Forms.Button();
            this.btnClanoviSaClanarinama = new System.Windows.Forms.Button();
            this.btnPrikaziRashod = new System.Windows.Forms.Button();
            this.btnPDF = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnDodajClana
            // 
            this.btnDodajClana.Location = new System.Drawing.Point(881, 51);
            this.btnDodajClana.Name = "btnDodajClana";
            this.btnDodajClana.Size = new System.Drawing.Size(135, 49);
            this.btnDodajClana.TabIndex = 0;
            this.btnDodajClana.Text = "Novi clan";
            this.btnDodajClana.UseVisualStyleBackColor = true;
            this.btnDodajClana.Click += new System.EventHandler(this.btnDodajClana_Click);
            // 
            // btnObrisiClana
            // 
            this.btnObrisiClana.Location = new System.Drawing.Point(881, 368);
            this.btnObrisiClana.Name = "btnObrisiClana";
            this.btnObrisiClana.Size = new System.Drawing.Size(135, 49);
            this.btnObrisiClana.TabIndex = 1;
            this.btnObrisiClana.Text = "Obrisi clana";
            this.btnObrisiClana.UseVisualStyleBackColor = true;
            this.btnObrisiClana.Click += new System.EventHandler(this.btnObrisiClana_Click);
            // 
            // btnIzmeniClana
            // 
            this.btnIzmeniClana.Location = new System.Drawing.Point(881, 219);
            this.btnIzmeniClana.Name = "btnIzmeniClana";
            this.btnIzmeniClana.Size = new System.Drawing.Size(135, 49);
            this.btnIzmeniClana.TabIndex = 2;
            this.btnIzmeniClana.Text = "Izmeni clana";
            this.btnIzmeniClana.UseVisualStyleBackColor = true;
            this.btnIzmeniClana.Click += new System.EventHandler(this.btnIzmeniClana_Click);
            // 
            // btnDodajPrihod
            // 
            this.btnDodajPrihod.Location = new System.Drawing.Point(41, 40);
            this.btnDodajPrihod.Name = "btnDodajPrihod";
            this.btnDodajPrihod.Size = new System.Drawing.Size(148, 39);
            this.btnDodajPrihod.TabIndex = 5;
            this.btnDodajPrihod.Text = "Novi prihod";
            this.btnDodajPrihod.UseVisualStyleBackColor = true;
            this.btnDodajPrihod.Click += new System.EventHandler(this.btnDodajPrihod_Click);
            // 
            // btnDodajRashod
            // 
            this.btnDodajRashod.Location = new System.Drawing.Point(41, 368);
            this.btnDodajRashod.Name = "btnDodajRashod";
            this.btnDodajRashod.Size = new System.Drawing.Size(148, 40);
            this.btnDodajRashod.TabIndex = 7;
            this.btnDodajRashod.Text = "Novi rashod";
            this.btnDodajRashod.UseVisualStyleBackColor = true;
            this.btnDodajRashod.Click += new System.EventHandler(this.btnDodajRashod_Click);
            // 
            // btnPrikaziClanarine
            // 
            this.btnPrikaziClanarine.Location = new System.Drawing.Point(41, 151);
            this.btnPrikaziClanarine.Name = "btnPrikaziClanarine";
            this.btnPrikaziClanarine.Size = new System.Drawing.Size(148, 42);
            this.btnPrikaziClanarine.TabIndex = 8;
            this.btnPrikaziClanarine.Text = "Prikazi Clanarine";
            this.btnPrikaziClanarine.UseVisualStyleBackColor = true;
            this.btnPrikaziClanarine.Click += new System.EventHandler(this.btnPrikaziClanarine_Click);
            // 
            // btnClanoviSaClanarinama
            // 
            this.btnClanoviSaClanarinama.Location = new System.Drawing.Point(331, 125);
            this.btnClanoviSaClanarinama.Name = "btnClanoviSaClanarinama";
            this.btnClanoviSaClanarinama.Size = new System.Drawing.Size(157, 52);
            this.btnClanoviSaClanarinama.TabIndex = 10;
            this.btnClanoviSaClanarinama.Text = "Prikazi clanove sa clanarinama";
            this.btnClanoviSaClanarinama.UseVisualStyleBackColor = true;
            this.btnClanoviSaClanarinama.Click += new System.EventHandler(this.btnClanoviSaClanarinama_Click);
            // 
            // btnPrikaziRashod
            // 
            this.btnPrikaziRashod.Location = new System.Drawing.Point(41, 473);
            this.btnPrikaziRashod.Name = "btnPrikaziRashod";
            this.btnPrikaziRashod.Size = new System.Drawing.Size(148, 38);
            this.btnPrikaziRashod.TabIndex = 11;
            this.btnPrikaziRashod.Text = "Prikazi Rashode";
            this.btnPrikaziRashod.UseVisualStyleBackColor = true;
            this.btnPrikaziRashod.Click += new System.EventHandler(this.btnPrikaziRashod_Click);
            // 
            // btnPDF
            // 
            this.btnPDF.Location = new System.Drawing.Point(331, 234);
            this.btnPDF.Name = "btnPDF";
            this.btnPDF.Size = new System.Drawing.Size(149, 48);
            this.btnPDF.TabIndex = 12;
            this.btnPDF.Text = "Izvod PDF";
            this.btnPDF.UseVisualStyleBackColor = true;
            this.btnPDF.Click += new System.EventHandler(this.btnPDF_Click);
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.ClientSize = new System.Drawing.Size(1141, 577);
            this.Controls.Add(this.btnPDF);
            this.Controls.Add(this.btnPrikaziRashod);
            this.Controls.Add(this.btnClanoviSaClanarinama);
            this.Controls.Add(this.btnPrikaziClanarine);
            this.Controls.Add(this.btnDodajRashod);
            this.Controls.Add(this.btnDodajPrihod);
            this.Controls.Add(this.btnIzmeniClana);
            this.Controls.Add(this.btnObrisiClana);
            this.Controls.Add(this.btnDodajClana);
            this.DoubleBuffered = true;
            this.Name = "frmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Finansije Sportskog Kluba";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnDodajClana;
        private System.Windows.Forms.Button btnObrisiClana;
        private System.Windows.Forms.Button btnIzmeniClana;
        private System.Windows.Forms.Button btnDodajPrihod;
        private System.Windows.Forms.Button btnDodajRashod;
        private System.Windows.Forms.Button btnPrikaziClanarine;
        private System.Windows.Forms.Button btnClanoviSaClanarinama;
        private System.Windows.Forms.Button btnPrikaziRashod;
        private System.Windows.Forms.Button btnPDF;
    }
}

