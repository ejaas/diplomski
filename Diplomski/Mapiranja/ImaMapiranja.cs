﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Diplomski.Entiteti;
using System.Windows.Forms;
using FluentNHibernate.Mapping;

namespace Diplomski.Mapiranja
{
    public class ImaMapiranja : ClassMap<Ima>
    {
        public ImaMapiranja()
        {
            try
            {
                //Mapiranje tabele
                Table("IMA");

                //Mapiranje propertija
                Map(x => x.BrClana, "BR_CLAN");
                Map(x => x.BrClanarine, "BR_CLANARINE");
                Map(x => x.BrRacuna, "BR_RACUNA");

                //Mapiranje primarnog kljuca
                Id(x => x.IdIma, "ID_IMA").GeneratedBy.TriggerIdentity();

              
            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.ToString());
            }

        }
    }
}
