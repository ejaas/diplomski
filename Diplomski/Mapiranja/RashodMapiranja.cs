﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Diplomski.Entiteti;
using FluentNHibernate.Mapping;
using System.Windows.Forms;

namespace Diplomski.Mapiranja
{
    public class RashodMapiranja : ClassMap<Rashod>
    {
         public RashodMapiranja()
        {
            try
            {

                //Mapiranje tabele
                Table("RASHOD");

                //Mapiranje primarnog kljuca

                Id(x => x.IdRashoda, "IDRASHODA").GeneratedBy.TriggerIdentity();

                //Mapiranje propertija
                Map(x => x.Tip_Rashoda, "TIP_RASHODA");
                Map(x => x.Cena, "CENA");
                Map(x => x.Izdavaoc, "IZDAVAOC");
                Map(x => x.DatumIzdavanja, "DATUM_IZDAVANJA");

            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.ToString());
            }
        }
    }
}
