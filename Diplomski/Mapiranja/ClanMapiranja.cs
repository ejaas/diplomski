﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;
using Diplomski.Entiteti;
using System.Windows.Forms;

namespace Diplomski.Mapiranja
{
    public class ClanMapiranja : ClassMap<Clan>
    {
        public ClanMapiranja()
        {
            try
            {

                //Mapiranje tabele
                Table("CLAN");

                //Mapiranje primarnog kljuca

                Id(x => x.IdClana, "IDCLAN").GeneratedBy.TriggerIdentity();

                //Mapiranje propertija
                Map(x => x.Ime, "IME");
                Map(x => x.Prezime, "PREZIME");
                Map(x => x.Rod, "ROD");
                Map(x => x.ImeRoditelja, "IMERODITELJA");
                Map(x => x.Email, "EMAIL");
                Map(x => x.Telefon, "TELEFON");
                Map(x => x.Adresa, "ADRESA");
                Map(x => x.Jmbg, "JMBG");
                Map(x => x.BrLicneKarte, "BRLICNEKARTE");

                HasManyToMany(x => x.Prihodi)
                    .Table("IMA")
                    .ParentKeyColumn("BR_CLAN")
                    .ChildKeyColumn("BR_CLANARINE")
                    .Cascade.All();

                HasMany(x => x.ImaPrihode).KeyColumn("BR_CLAN").LazyLoad().Cascade.All().Inverse();
            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.ToString());
            }
        }

    }

}
