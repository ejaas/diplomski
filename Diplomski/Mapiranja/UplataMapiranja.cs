﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Diplomski.Entiteti;
using System.Windows.Forms;
using FluentNHibernate.Mapping;

namespace Diplomski.Mapiranja
{
    public class UplataMapiranja: ClassMap<Uplata>
    {
        public UplataMapiranja()
        {
            try
            {
                //Mapiranje tabele
                Table("UPLATA");

                //Mapiranje propertija
                Map(x => x.Ime_Clan, "IME_CLAN");
                Map(x => x.Cena, "CENA");
                Map(x => x.Br_Racuna, "BR_RACUNA");
                Map(x => x.Datum, "DATUM");
                // Map(x => x.Id_Uplate, "ID_UPLATE");

                //Mapiranje primarnog kljuca
                Id(x => x.Id_Uplate, "ID_UPLATE").GeneratedBy.TriggerIdentity();


            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.ToString());
            }

        }
    }
}
