﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Diplomski.Entiteti;
using FluentNHibernate.Mapping;
using System.Windows.Forms;


namespace Diplomski.Mapiranja
{
    public class PrihodMapiranja : ClassMap<Prihod>
    {
        public PrihodMapiranja()
        {
            //Mapiranje tabele
            Table("PRIHOD");

            /*mapiranje podklasa. Ova fja kaze da ce kolona sa imenom 
             'TIP' da odredi sa kojom podklasom radimo*/
            DiscriminateSubClassesOnColumn("TIP_PRIHODA");

            //mapiranje primarnog kljuca
            Id(x => x.IdPrihoda, "IDPRIHODA").GeneratedBy.TriggerIdentity();

            //mapiranje svojstava
            //Map(x => x.Tip, "TIP");

            HasManyToMany(x => x.Clanovi)
                    .Table("IMA")
                    .ParentKeyColumn("BR_CLANARINE")
                    .ChildKeyColumn("BR_CLAN")
                    .Cascade.All()
                    .Inverse();

            HasMany(x => x.ImaClanove).KeyColumn("BR_CLANARINE").LazyLoad().Cascade.All().Inverse();

        }
    }


    class ClanarinaMapiranja : SubclassMap<Prihod.Clanarina>
    {
        public ClanarinaMapiranja()
        {
        
            DiscriminatorValue("Clanarina");
            //Mapiramo proste atribute podklase ako ih ima
            Map(x => x.Tip_Clanarine, "TIP_CLANARINE");
            Map(x => x.Cena_Clanarine, "CENA_CLANARINE");
            Map(x => x.Vreme_Vazenja, "VREME_VAZENJA");
            Map(x => x.Datum_Pocetka, "DATUM_POCETKA");
            Map(x => x.Datum_Isteka, "DATUM_ISTEKA");

        }
    }

    class UslugaMapiranja : SubclassMap<Prihod.Usluga>
    {
        public UslugaMapiranja()
        {

            DiscriminatorValue("Usluga");
            //Mapiramo proste atribute podklase ako ih ima
            Map(x => x.Tip_Usluge, "TIP_USLUGE");
           

        }
    }
    class PublikacijaMapiranja : SubclassMap<Prihod.Publikacija>
    {
        public PublikacijaMapiranja()
        {

            DiscriminatorValue("Publikacija");
            //Mapiramo proste atribute podklase ako ih ima
            Map(x => x.Tip_Publikacije, "TIP_PUBLIKACIJE");


        }
    }

    class DonacijaMapiranja : SubclassMap<Prihod.Donacija>
    {
        public DonacijaMapiranja()
        {

            DiscriminatorValue("Donacija");
            //Mapiramo proste atribute podklase ako ih ima
            Map(x => x.Tip_Donacije, "TIP_DONACIJE");


        }
    }
}
