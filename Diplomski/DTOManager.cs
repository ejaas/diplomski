﻿using Diplomski.Entiteti;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static Diplomski.DTOs;

namespace Diplomski
{
    public class DTOManager
    {
        public static IList<ClanInfo> VratiClanove()
        {
            try
            {
                ISession s = DataLayer.GetSession();
                IList<Clan> clanovi = s.QueryOver<Clan>()
                                .List<Clan>();

                IList<ClanInfo> clanoviInfo = new List<ClanInfo>();
                foreach (Clan k in clanovi)
                {
                    clanoviInfo.Add(new ClanInfo(k.IdClana, k.Ime, k.Prezime, k.Email, k.Telefon, k.Adresa, k.Rod, k.Jmbg, k.BrLicneKarte, k.ImeRoditelja));
                }

                s.Close();

                return clanoviInfo;

            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.ToString());
                return null;
            }
        }

        public static IList<ClanoviZaBrisanje> VratiClanoveZaBrisanjeDTO()
        {
            try
            {
                IList<ClanoviZaBrisanje> saloni = new List<ClanoviZaBrisanje>();
                ISession s = DataLayer.GetSession();
                IList<Clan> cla = s.QueryOver<Clan>()
                                .List<Clan>();

                foreach (Clan clan in cla)
                {
                    saloni.Add(new ClanoviZaBrisanje(clan.IdClana, clan.Ime, clan.Prezime));
                }

                s.Close();

                return saloni;

            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.ToString());
                return null;
            }

        }


        public static IList<ClanarinaInfo> VratiClanarine()
        {
            try
            {
                ISession s = DataLayer.GetSession();
                IList<Prihod.Clanarina> clanarina = s.QueryOver<Prihod.Clanarina>()
                                .List<Prihod.Clanarina>();

                IList<ClanarinaInfo> clanarinaInfo = new List<ClanarinaInfo>();
                foreach (Prihod.Clanarina k in clanarina)
                {
                    clanarinaInfo.Add(new ClanarinaInfo(k.IdPrihoda, k.Tip_Clanarine, k.Cena_Clanarine, k.Vreme_Vazenja, k.Datum_Pocetka, k.Datum_Isteka));
                }

                s.Close();

                return clanarinaInfo;

            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.ToString());
                return null;
            }
        }


        public static IList<RashodInfo> VratiRashode()
        {
            try
            {
                ISession s = DataLayer.GetSession();
                IList<Rashod> rashod = s.QueryOver<Rashod>()
                                .List<Rashod>();

                IList<RashodInfo> rashodInfo = new List<RashodInfo>();
                foreach (Rashod k in rashod)
                {
                    rashodInfo.Add(new RashodInfo(k.IdRashoda, k.Tip_Rashoda, k.Cena, k.Izdavaoc, k.DatumIzdavanja));
                }

                s.Close();

                return rashodInfo;

            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.ToString());
                return null;
            }
        }

        public static IList<ImaInfo> VratiVeza()
        {
            try
            {
                ISession s = DataLayer.GetSession();

                IList<Ima> ima = s.QueryOver<Ima>()
                                .List<Ima>();

                IList<ImaInfo> imaInfo = new List<ImaInfo>();

                foreach (Ima k in ima)
                {
                    imaInfo.Add(new ImaInfo(k.IdIma  , k.BrClanarine, k.BrRacuna, k.BrClana));
                }

                s.Close();

                return imaInfo;

            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.ToString());
                return null;
            }
        }


        public static void KreirajTipClanarine(Prihod.Clanarina t)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                s.Save(t);
                s.Flush();
                s.Close();
            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.ToString());
            }
        }
    }
}
