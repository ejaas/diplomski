﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Diplomski.Forme;
using FluentNHibernate.Mapping;
using Diplomski.Entiteti;
using NHibernate;



namespace Diplomski
{
    public partial class frmMain : Form
    {
        public frmMain()
        {
            InitializeComponent();
        }

        private void btnDodajClana_Click(object sender, EventArgs e)
        {
            DodajClana forma = new DodajClana();
            this.Visible = false;
            forma.ShowDialog();
            this.Visible = true;
            this.DialogResult = DialogResult.OK;
            
        }

        private void btnIzmeniClana_Click(object sender, EventArgs e)
        {
            ModifikujClana forma = new ModifikujClana();
            this.Visible = false;
            forma.ShowDialog();
            this.Visible = true;
            this.DialogResult = DialogResult.OK;
        }

        private void btnUcitajClana_Click(object sender, EventArgs e)
        {
            try
            {

                ISession i = DataLayer.GetSession();

                Diplomski.Entiteti.Clan c = i.Load<Diplomski.Entiteti.Clan>(127);

      

                MessageBox.Show("Ime: " + c.Ime + "   Prezime: " + c.Prezime + "   Jmbg:" + c.Jmbg);

                i.Close();
            }

            catch (Exception ec)
            {
                MessageBox.Show(ec.ToString());
            }
        }

        private void btnObrisiClana_Click(object sender, EventArgs e)
        {

            ObrisiClana forma = new ObrisiClana();
            this.Visible = false;
            forma.ShowDialog();
            this.Visible = true;
            this.DialogResult = DialogResult.OK;
        }

        private void btnDodajPrihod_Click(object sender, EventArgs e)
        {
            DodajPrihod forma = new DodajPrihod();
            this.Visible = false;
            forma.ShowDialog();
            this.Visible = true;
            this.DialogResult = DialogResult.OK;
        }

        private void btnDodajRashod_Click(object sender, EventArgs e)
        {
            DodajRashod forma = new DodajRashod();
            this.Visible = false;
            forma.ShowDialog();
            this.Visible = true;
            this.DialogResult = DialogResult.OK;
        }

        private void btnPrikaziClanarine_Click(object sender, EventArgs e)
        {
           PrikaziClanarinu forma = new PrikaziClanarinu();
            this.Visible = false;
            forma.ShowDialog();
            this.Visible = true;
            this.DialogResult = DialogResult.OK;
        }

        private void btnHasManytoMany_Click(object sender, EventArgs e)
        {

            try
            {
                ISession s = DataLayer.GetSession();

                Clan r1 = s.Load<Clan>(127);

                foreach (Entiteti.Prihod p1 in r1.Prihodi)
                {
                    MessageBox.Show(p1.Tip_Prihoda);
                }


                Entiteti.Prihod p2 = s.Load<Entiteti.Prihod>(22);

                foreach (Clan r2 in p2.Clanovi)
                {
                    MessageBox.Show(r2.Ime + " " + r2.Prezime);
                }

                s.Close();
            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }
        }

        private void btnClanoviSaClanarinama_Click(object sender, EventArgs e)
        {
            PrikaziSve forma = new PrikaziSve();
            this.Visible = false;
            forma.ShowDialog();
            this.Visible = true;
            this.DialogResult = DialogResult.OK;
        }

        private void btnPrikaziRashod_Click(object sender, EventArgs e)
        {
            PrikaziRashode forma = new PrikaziRashode();
            this.Visible = false;
            forma.ShowDialog();
            this.Visible = true;
            this.DialogResult = DialogResult.OK;
        }

        private void btnPDF_Click(object sender, EventArgs e)
        {
            PDF forma = new PDF();
            this.Visible = false;
            forma.ShowDialog();
            this.Visible = true;
            this.DialogResult = DialogResult.OK;
        }
    }
}
