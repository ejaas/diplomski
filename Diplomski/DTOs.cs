﻿using Diplomski.Entiteti;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System;


namespace Diplomski
{
    public class DTOs
    {

        public class ClanInfo
        {
            public int IdClan { get; set; }
            public string Ime { get; set; }
            public string Prezime { get; set; }
            public string Email { get; set; }
            public int Telefon { get; set; }
            public string Adresa { get; set; }

            public string Rod { get; set; }
            public int Jmbg { get; set; }
            public int brLk { get; set; }

            public string Imeroditelja { get; set; }

         
            public ClanInfo(int idClan, string ime, string prezime, string email,  int telefon , string adresa, string rod, int jmbg, int brlk, string imeroditelja)
            {
                this.IdClan = idClan;
                this.Ime = ime;
                this.Prezime = prezime;
                this.Email = email;
                this.Telefon = telefon;
                this.Adresa = adresa;
                this.Rod = rod;
                this.Jmbg = jmbg;
                this.brLk = brlk;
                this.Imeroditelja = imeroditelja;
            }
        }

        public class ClanoviZaBrisanje
        {
            public int IdClana { get; set; }
            public string Ime{ get; set; }

            public string Prezime { get; set; }

            public ClanoviZaBrisanje(int idClana, string ime, string prezime)
            {
                IdClana = idClana;
                Ime = ime;
                Prezime = prezime;
            }
        }


        public class ClanarinaInfo
        {
            public int IdPrihoda { get; set; }
            public string Tip_Clanarine { get; set; }
            public int Cena_Clanarine { get; set; }
            public int Vreme_Vazenja { get; set; }
            public DateTime Datum_Pocetka { get; set; }
            public DateTime Datum_Isteka { get; set; }
           

            public ClanarinaInfo(int idPrihoda, string tip_Clanarine, int cena_Clanarine, int vreme_Vazenja, DateTime datum_Pocetka, DateTime datum_Isteka)
            {
                this.IdPrihoda = idPrihoda;
                this.Tip_Clanarine = tip_Clanarine;
                this.Cena_Clanarine = cena_Clanarine;
                this.Vreme_Vazenja = vreme_Vazenja;
                this.Datum_Pocetka = datum_Pocetka;
                this.Datum_Isteka = datum_Isteka;
               
                
            }
        }

        public class ImaInfo
        {
            public int Id_Ima { get; set; }
            public int Br_Clan { get; set; }
            public int Br_Clanarine { get; set; }

            public int Br_Racuna { get; set; }
            


            public ImaInfo(  int id_Ima, int br_Clanarine, int br_Racuna, int br_Clan)
            {
                this.Id_Ima = id_Ima;
                this.Br_Clanarine = br_Clanarine;
                this.Br_Clan = br_Clan;
                this.Br_Racuna = br_Racuna;
                

            }
        }

        public class RashodInfo
        {
            public int IdRashoda { get; set; }
            public string Tip_Rashoda { get; set; }
            public int Cena { get; set; }

            public string Izdavaoc { get; set; }

            public DateTime Datum_Izdavanja { get; set; }



            public RashodInfo(int idRashoda, string tip_Rashoda, int cena, string izdavaoc, DateTime datum_Izdavanja)
            {

                this.IdRashoda = idRashoda;
                this.Tip_Rashoda = tip_Rashoda;
                this.Cena = cena;
                this.Izdavaoc = izdavaoc;
                this.Datum_Izdavanja = datum_Izdavanja;


            }
        }
    }
}
